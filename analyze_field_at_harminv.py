import plotparams
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from params import load_useparams, get_cell_dimensions

import numpy as np
import h5py
import pandas as pd

def main():
    print('ANALYZING FREQUENCIES')
    
    print('- reading sim parameters...')

    params = load_useparams('output/params_used.json')
    FCEN = params['fcen']
    DF = params['df']
    
    delta_t = 1./(FCEN*10.)
    
    
    cell_dx, cell_dy = get_cell_dimensions(params=params, withpml=False)
    
    
    print('- reading field data...')
    
    data = np.array(h5py.File('output/main-field_at_harminv.h5')['ez'])
    
    
    fft = np.fft.rfft(data, axis=1)
    fftabs = np.abs(fft)
    fftabs = fftabs / np.max(fftabs)
    fftfreq = np.fft.rfftfreq(data.shape[1], d=delta_t)

    
    # print('- reading harminv csv')
    
    # harminv_data = pd.read_csv('output/resonances_log.csv', sep=', ', engine='python')
    # harminv_frequencies = harminv_data['frequency'].values
    # harminv_qualities = harminv_data['Q'].values

    if False:
        print('- plotting data...')

        fig, ax = plt.subplots()

        t_list = np.arange(data.size) * delta_t

        ax.plot(
            t_list,
            data / np.max(np.abs(data))
            )

        ax.set_xlabel('# timesteps')
        ax.set_ylabel('field strength')

        fig.savefig('output/field_analysis_field.png')
        plt.close(fig)



    print('- plotting trafo...')

    fig, ax = plt.subplots()

    im = ax.imshow(
        fftabs, 
        aspect='auto',
        extent=[
            fftfreq[0],fftfreq[-1],
            -cell_dy*.5,cell_dy*.5
            ],
        norm=LogNorm(vmin=1e-3, vmax=1)
        )
    plt.colorbar(im, label='normed fourier coeff.')

    ax.set_xlabel('frequency')
    ax.set_ylabel('$y$')
    
    # for f, quality in zip(harminv_frequencies, harminv_qualities):
    #     if abs(quality) > 1e3:
    #         ax.axvline(f, zorder=0, color='lightgray')

    # ax.axvline(FCEN, color='black', linestyle='dashed', zorder=0)

    ax.set_xlim(0,FCEN+0.5)

    fig.savefig('output/field_analysis_fft.png')
    plt.close(fig)
    
    
    print('Done!')

if __name__ == '__main__':
    main()