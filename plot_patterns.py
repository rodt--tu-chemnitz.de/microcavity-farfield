import params as pr
import plotparams

from limacon_class import Limacon
import meep as mp

from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt
import numpy as np


def main():
    print('Plotting all timesteps')

    print('\t- load params')
    
    params = pr.load_useparams(fname='output/params_used.json')
    
    OUTPUTDIR = params["outputfolder"]

    # DPML = params['dpml']
    DPML = 0. # not shown in file
    RESOLUTION = params['resolution']

    RADIUS = params['radius']
    DEFORM = params['deform']
    DIST_NFB = params['dist_nfb']
    PAD = params['pad']

    cmap = plotparams.cmap

    # dx = dy = (RADIUS + DIST_NFB + PAD + DPML)*2.

    dx, dy = pr.get_cell_dimensions(params=params, withpml=False)

    # paths
    x_factorlist = np.array([1,1,-1,-1,1])
    y_factorlist = np.array([1,-1,-1,1,1])

    # path for DIST_NFB
    x_pos_nfb = dx*.5 - (DPML + PAD)
    y_pos_nfb = dy*.5 - (DPML + PAD)
    path_nfb = np.array([
        x_factorlist * x_pos_nfb,
        y_factorlist * y_pos_nfb
        ])
    

    # path for PML
    # inner
    x_pos_pml_inner = dx*.5 - DPML
    y_pos_pml_inner = dy*.5 - DPML
    path_pml_inner = np.array([
        x_factorlist * x_pos_pml_inner,
        y_factorlist * y_pos_pml_inner
        ])
    # outer
    x_pos_pml_outer = dx*.5
    y_pos_pml_outer = dy*.5
    path_pml_outer = np.array([
        x_factorlist * x_pos_pml_outer,
        y_factorlist * y_pos_pml_outer
        ])


    print('\t- load data')

    eps_filename = OUTPUTDIR + 'geometry.dat'
    data_eps = np.loadtxt(eps_filename)
    


    for fname in ['geometry', 'pattern_start', 'pattern_end']:
        print(fname)

        # initialize
        fig, ax = plt.subplots(figsize=(6,6))

        extent=np.array([-dx,dx,-dy,dy])*.5

        ax.imshow(
            data_eps,
            cmap='gist_gray',
            extent=extent
            )

        if fname != 'geometry':

            ez_filename = fname + '.dat'
            ez_data = np.loadtxt(OUTPUTDIR + ez_filename)

            ax.imshow(
                np.abs(ez_data)**2,
                cmap=cmap,
                alpha=0.8,
                extent=extent
                )
        
        ax.plot(
            *path_nfb,
            color='black',
            linestyle='--'
            )
        
        if False: # draw circle for comparison
            ax.axvline(0)
            ax.axhline(0)
            
            lim = Limacon(
                radius=params['radius'],
                deform=DEFORM,
                material=mp.Medium(index=params['index'])
                )
            radius_equicircle = lim.maxlateral
            
            phi_list = np.linspace(0,1,51) * 2. * np.pi
            x_list = np.cos(phi_list) * radius_equicircle
            y_list = np.sin(phi_list) * radius_equicircle
            
            ax.plot(
                x_list,y_list,
                # marker='o',
                color='tab:green'
                )
        
        
        # ax.plot(
        #     *path_pml_inner,
        #     color='tab:blue'
        #     )
        # ax.plot(
        #     *path_pml_outer,
        #     color='tab:blue'
        #     )
        
        # create
        vertices_inner = path_pml_inner.T
        vertices_outer = np.flip(path_pml_outer.T, axis=0)
        codes_inner = [Path.LINETO for v in vertices_inner]
        codes_inner[0] = Path.MOVETO
        codes_outer = [Path.LINETO for v in vertices_outer]
        codes_outer[0] = Path.MOVETO
        
        vertices = np.concatenate((vertices_inner, vertices_outer))
        codes = codes_inner + codes_outer
        path = Path(vertices, codes)
        patch = PathPatch(
            path,
            facecolor='None',
            edgecolor='black',
            hatch='//',
            linewidth=3.
            )

        ax.add_patch(patch)
        

        # for x in [-RADIUS, RADIUS]:
        #     linedict=dict(
        #         color='black',
        #         linestyle='dashed'
        #         )
        #     ax.axvline(x, **linedict)
        #     ax.axhline(x, **linedict)

        # ax.set_axis_off()

        ax.tick_params(
            direction='inout'
            )

        ax.set_xlabel('$x$')
        ax.set_ylabel('$y$')

        fig.savefig(OUTPUTDIR + fname + '.png',dpi=300, bbox_inches='tight')
        plt.close(fig)

        # return


if __name__ == '__main__':
    main()