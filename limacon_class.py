'''
This file contains the limacon subclass, which
is essentially a
'''

from meep import Prism, Vector3, Medium
import numpy as np
import pandas

class Limacon():
    def __init__(
        self, 
        radius,             # mean radius
        deform,             # deformation parameter
        material,           # refractive index
        center=Vector3(),        # center of grafvity
        n_edgepoints=50,   # number of edgepoints used for construction
        height=1,           # height, doesn't matter in 2D
        phi0=0,             # angle of limacon
        phi_of_s_path='notebooks/phi_of_s.csv'
        ):

        ###############################################################
        # initialize own basic attributes
        ###############################################################

        self.radius = radius
        self.deform = deform
        self.material = material
        self.center = center
        self.n_edgepoints = n_edgepoints
        self.height = height


        ###############################################################
        # attributes to shift vertices so it matches with meeps shift
        ###############################################################

        # max lateral span (looks complicated, but is calculated with sympy)
        self.maxlateral = 2*radius*np.sqrt(-(-3*deform + np.sqrt(8*deform**2 + 1))/(deform - 1))*(deform - 1)**2*(-2*deform + np.sqrt(8*deform**2 + 1) + 1)/(-4*deform + np.sqrt(8*deform**2 + 1) + 1)**2
        # x coord for max lateral span (if phi0 = 0)
        self.maxlateral_xpos = radius*(deform - 1)*(-2*deform + np.sqrt(8*deform**2 + 1) - 1)*(-2*deform + np.sqrt(8*deform**2 + 1) + 1)/(-4*deform + np.sqrt(8*deform**2 + 1) + 1)**2
        # center of equivalent circle
        self.center_of_equicircle = Vector3(np.cos(phi0), np.sin(phi0)) * self.maxlateral_xpos * radius

        # construct edge vertices
        self.phi_list = np.arange(self.n_edgepoints) * 2. * np.pi / self.n_edgepoints
        
        x_list = self.x(self.phi_list)
        y_list = self.y(self.phi_list)
        self.center_of_mass = Vector3( # by default of meep, center of mass is in (0,0)
            np.mean(x_list),
            np.mean(y_list)
            )

        self.centershift = self.center_of_mass - self.center_of_equicircle # calculate necessary shift

        self._vertice_list_vector3 = [Vector3(x,y) for x,y in zip(x_list, y_list)]

        self.vertice_list_centershift = - np.array(self.center + self.center_of_equicircle)[:2] # vertices need to be shifted by this much to coincide with meep shift
        self.vertice_list = np.array([[v.x, v.y] for v in self._vertice_list_vector3]) + self.vertice_list_centershift



        ###############################################################
        # attributes for husimi function construction
        ###############################################################
        
        try:
            # angles for equidistant borders
            df = pandas.read_csv(phi_of_s_path)
            eps_str = str('{0:.2f}'.format(self.deform))

            self.equidist_s_slist = df['s'].values
            self.equidist_s_philist = df[eps_str].values

            x_list = self.x(self.equidist_s_philist)
            y_list = self.y(self.equidist_s_philist)
            self.equidist_s_verticelist = np.transpose([x_list,y_list]) + self.vertice_list_centershift
        except FileNotFoundError:
            print('LIMACON CLASS WARNING: phi_of_s.csv not found -> list of equidistant borderpoints not available')



        
    def radius_at_angle(self, phi:np.ndarray) -> np.ndarray:
        '''
        Gives R at phi
        '''
        return self.radius * (1. + self.deform * np.cos(phi))
    
    
    
    def x(self, phi:np.ndarray) -> np.ndarray:
        '''
        gives x-coordinate at angle phi
        '''
        return self.radius_at_angle(phi) * np.cos(phi)
    
    
    def y(self, phi:np.ndarray) -> np.ndarray:
        '''
        gives y-coordinate at angle phi
        '''
        return self.radius_at_angle(phi) * np.sin(phi)
    
    
    def x_shifted(self, phi:np.ndarray) -> np.ndarray:
        '''
        gives x-coordinate at angle phi
        '''
        return self.radius_at_angle(phi) * np.cos(phi) + self.vertice_list_centershift[0]
    
    
    def y_shifted(self, phi:np.ndarray) -> np.ndarray:
        '''
        gives y-coordinate at angle phi
        '''
        return self.radius_at_angle(phi) * np.sin(phi) + self.vertice_list_centershift[1]


    @property
    def Prism(self) -> Prism:
        return Prism(
            self._vertice_list_vector3, 
            self.height, 
            center=self.center+self.centershift, 
            material=self.material
            )
    

        
    def tangent_vector(self, phi:np.ndarray) -> np.ndarray:
        '''
        Gives point normed tangent vector at on phi
        '''
        
        norm_factor = 1. / np.sqrt( self.deform**2 + 2*self.deform*np.cos(phi) + 1 )
        dx = norm_factor * -(self.deform * np.sin(2*phi) + np.sin(phi))
        dy = norm_factor * (-2*self.deform * np.sin(phi)**2 + self.deform + np.cos(phi))
        
        return np.transpose([dx, dy])
    


    def normal_vector(self, phi:np.ndarray) -> np.ndarray:
        '''
        Gives normed normal vector pointing outside at phi by using the tangent vector
        '''
        dx_tangent, dy_tangent = self.tangent_vector(phi).T
        return np.transpose([dy_tangent, -dx_tangent])
        