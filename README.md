# Microcavity Farfield

FDTD simulations using meep are utilized to calculate the farfield of single microcavities.


## Files

**Python:**
- `main.py`: Performs FDTD calculations with `meep`.
- `plot_farfield.py`: loads `farfield.dat` and plots it in kartesian and polar representations
- `plot_timesteps.py`: reads `h5` files concerning the geometry and the fields and plots all timesteps, so the images can be put together into a GIF.

**JSON:**
- `parameters.json`: contains all input for the calculation.

**SHELL:**
- `execall.sh`: executes `main.py` and saves console output in `log.log`, as well as plots the farfield and creates a GIF.
- `creategif.sh`: Plots all fields using `plot_timesteps.py` and creates gif from them with `convert`.
- `varyparams.sh`: Varies an input parameter in `main.py` by passing it as `argv`.


## What does the code do?

1. activate Gaussian source
2. wait until it is turned off again
3. add nearfield box
4. perform FDTD iterations for an amount of time steps
5. save farfield 