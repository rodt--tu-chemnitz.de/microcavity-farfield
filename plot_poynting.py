import matplotlib
import params as pr
import plotparams
from limacon_class import Limacon

import matplotlib.pyplot as plt
import h5py

import numpy as np
from numpy.linalg import norm

import os
from scipy.interpolate import RegularGridInterpolator


def main():
    print('Plotting all Poynting Vectors at all timesteps')
    
    ########################################
    # 1. loading parameters
    ########################################

    print('\t- load params')
    
    params = pr.load_useparams('output/params_used.json')
    
    OUTPUTDIR = params['outputfolder']

    DPML = params['dpml']
    RESOLUTION = params['resolution']

    RADIUS = params['radius']
    DIST_NFB = params['dist_nfb']
    PAD = params['pad']
    
    N_CAVITIES = params['n_cavities']
    DEFORM = params['deform']
    
    COMPONENT = params['component']
    

    dx, dy = pr.get_cell_dimensions(params=params, withpml=False)

    # paths
    x_factorlist = np.array([1,1,-1,-1,1])
    y_factorlist = np.array([1,-1,-1,1,1])

    # path for DIST_NFB
    x_pos_nfb = dx*.5 - (DPML + PAD)
    y_pos_nfb = dy*.5 - (DPML + PAD)
    path_nfb = np.array([
        x_factorlist * x_pos_nfb,
        y_factorlist * y_pos_nfb
        ])
    

    # path for PML
    # inner
    x_pos_pml_inner = dx*.5 - DPML
    y_pos_pml_inner = dy*.5 - DPML
    path_pml_inner = np.array([
        x_factorlist * x_pos_pml_inner,
        y_factorlist * y_pos_pml_inner
        ])
    # outer
    x_pos_pml_outer = dx*.5
    y_pos_pml_outer = dy*.5

    ########################################
    # 2. loading and analysing data
    ########################################

    print('\t- load data')

    # eps_filename must be determined like this because the
    # name of the file is kinda random 
    eps_filename = [
        f for f in os.listdir(OUTPUTDIR)
        if f[-3:] == '.h5' and f[:9] == 'main-eps-'
        ][0]
    
    eps_fullname = OUTPUTDIR + eps_filename
    print('\t\t-> reading ' + eps_fullname)
    eps_h5file = h5py.File(eps_fullname, 'r')
    data_eps = np.transpose(eps_h5file['eps'])

    field_fullname = OUTPUTDIR + 'main-mode_period.h5'
    print('\t\t-> reading ' + field_fullname)
    field_h5file = h5py.File(field_fullname, 'r')

    poynting_x = field_h5file[ 'sx' ]
    poynting_y = field_h5file[ 'sy' ]
    poynting_z = field_h5file[ 'sz' ]

    n_timesteps = poynting_x.shape[2]
    print('\t\t-> data contains {0} timesteps'.format(n_timesteps))


    # print(data_field_z.shape)
    print('\t\tscaled with res:', np.array(poynting_x.shape)[:2] / RESOLUTION)
    print('\t\tcompare to expected dx, dy:',dx,dy)
    
    
    
    # ez_field
    
    field_z = field_h5file[ COMPONENT ]
    
    # only search one column/row, so it doesn't take ages
    print('\t\t-> searching maximum absolute field')
    searchcolumn = field_z.shape[0] // 2   # true division so index stays integer
    field_z_absmax = np.max(np.abs(field_z[searchcolumn, :, :]))
    
    
    ########################################
    # 4. analyzing poynting vectors
    ########################################

    print('\t- constructing limacon coords')
    
    # get centerlist of limacons

    limacon_centerdist = pr.get_limacon_centerdist(params=params)
    center_ylist = np.array([limacon_centerdist * i for i in range(N_CAVITIES)])
    center_ylist = center_ylist - np.mean(center_ylist)

        
    l = Limacon(
        RADIUS*0.99, 
        DEFORM, 
        None,
        n_edgepoints=200
        )
    
    # read interpolated data
    
    # read radial quantumnumber so we get a vector at each
    radial_quantumnumber = np.loadtxt(
        OUTPUTDIR + 'radial_quantumnumber.dat',
        dtype=int
        )
    
    n_evalcoords_per_cavity = radial_quantumnumber * 2
    evalcoords_all = l.equidist_s_verticelist
    evalcoords_selection = np.array([
        evalcoords_all[ round(x) ] 
        for x in 
        np.arange(n_evalcoords_per_cavity) / n_evalcoords_per_cavity * evalcoords_all.shape[0]
        ])
    
    
    
    print('\t- analyzing poynting vectors')
    
    p_coord_listlist = []
    p_vector_listlist = []
    
    field_z_absmean = None
    
    for i in range(n_timesteps):

        
        # grabbing right data
        px_now = poynting_x[:,:,i].T
        py_now = poynting_y[:,:,i].T
        pz_now = poynting_z[:,:,i].T
        
        field_z_now = field_z[:,:,i].T 

        # adding field for meaning purposes
        field_z_now_abs = np.abs(field_z_now)
        if field_z_absmean is None:
            field_z_absmean = field_z_now_abs
        else:
            field_z_absmean += field_z_now_abs

        # create interpolating function
        x_coords = np.linspace(-dx*.5, dx*.5, px_now.shape[1])
        y_coords = np.linspace(-dy*.5, dy*.5, px_now.shape[0])

        p_function_x = RegularGridInterpolator((x_coords, y_coords), px_now.T)
        p_function_y = RegularGridInterpolator((x_coords, y_coords), py_now.T)
        p_function_z = RegularGridInterpolator((x_coords, y_coords), pz_now.T)
        

        # plotting

        
        # draw poynting vectors
        
        p_coord_list = []
        p_vector_list = []
        
        for y in center_ylist:
            p_coords_now = evalcoords_selection + np.array([0,y])
            x_values = p_function_x(p_coords_now)
            y_values = p_function_y(p_coords_now)
            p_vectors_now = np.transpose([x_values, y_values])
            
            p_coord_list.append(p_coords_now)
            p_vector_list.append(p_vectors_now)
        
        p_coord_list = np.concatenate(p_coord_list)
        p_vector_list = np.concatenate(p_vector_list)
        
        p_coord_listlist.append(p_coord_list)
        p_vector_listlist.append(p_vector_list)

    
    # maximum norm of vectors for scaling purposes
    p_vector_maxnorm = np.max( norm(np.concatenate(p_vector_listlist), axis=1) )
    print('\t\t-> ||s_max|| = {0}'.format(p_vector_maxnorm))
    
    # terminating meaning of field
    field_z_mean = field_z_absmean / n_timesteps
    
    
    ########################################
    # 5. plotting
    ########################################


    print('\t- plotting')


    # get to display the progress percentage
    partdone_list = (np.arange(n_timesteps) + 1) / n_timesteps
    partdone_wants = np.arange(0,1.05,0.05)
    n_wants = len(partdone_wants)

    percentage_indices = []
    for want in partdone_wants: # get indices, at which the percentage is to be displayed
        templist= np.abs(partdone_list - want)
        argmin = np.argsort(templist)[0]
        percentage_indices.append(argmin)

    search_index_index = 0
    search_index_now = percentage_indices[search_index_index]
    
    for i, (p_coord_list, p_vector_list) in enumerate(zip(p_coord_listlist, p_vector_listlist)):
        if i == search_index_now:
            print('\t\t{0:.0f} % done'.format(partdone_list[i]*100.))
            
            search_index_index = search_index_index+1
            if search_index_index < n_wants:
                search_index_now = percentage_indices[search_index_index]

        
        # grabbing right data
        
        field_z_now = field_z[:,:,i].T 
        

        # plotting


        fig, ax = plt.subplots()
        

        extent=np.array([-dx,dx,-dy,dy])*.5

        ax.imshow(
            data_eps,
            cmap='binary',
            extent=extent
            )

        im = ax.imshow(
            field_z_now,
            cmap='coolwarm',
            alpha=0.9,
            extent=extent,
            # norm=matplotlib.colors.LogNorm(
            #     vmin=1e-3,
            #     vmax=1
            #     ),
            vmin=-field_z_absmax,
            vmax=field_z_absmax
            )
            
        
        # draw poynting vectors
        
        
        
        for p_coord, p_vector in zip(p_coord_list, p_vector_list):
            r0 = p_coord
            dr = p_vector / p_vector_maxnorm * 3. # vector length = [0, 3]
            
            dr_abs = norm(dr)
            if dr_abs < 0.1:
                continue
            else:
                ax.arrow(
                    *r0, 
                    *dr*.5, 
                    color='black',
                    length_includes_head=True,
                    head_width=0.1,
                    alpha=0.5
                    )


        ax.set_axis_off()
        ax.set_aspect('equal')

        fig.savefig('TEMP/img_{0:06d}.png'.format(i),dpi=300, bbox_inches='tight')
        plt.close(fig)
    
    ########################################
    # 6. analyzing poynting vectors densely
    ########################################

    print('\t- dense analysis of poynting vectors')
    
    # read interpolated data
    n_evalcoords_per_cavity = 2**8
    evalcoords_all = l.equidist_s_verticelist
    evals_all = l.equidist_s_slist
    
    # indices where to evaluate
    eval_indices = np.array([
        round(x) for x in 
        np.arange(n_evalcoords_per_cavity) / n_evalcoords_per_cavity * evalcoords_all.shape[0]
        ])
    evalcoords_selection = evalcoords_all[eval_indices]
    evals_all_selection = evals_all[eval_indices]
    
    # get coordinates where to evaluate
    p_coord_list = []
    p_s_list = []
    for i, y in enumerate(center_ylist):
        # coordinates
        p_coords_now = evalcoords_selection + np.array([0,y])
        p_coord_list.append(p_coords_now)
        
        # path on border
        p_s_now = evals_all_selection + i
        p_s_list.append(p_s_now)
    
    p_coord_list = np.concatenate(p_coord_list)
    p_s_list = np.concatenate(p_s_list)
    
    
    p_vectornorm_listlist = []
    for i in range(n_timesteps):
        
        # grabbing right data
        px_now = poynting_x[:,:,i].T
        py_now = poynting_y[:,:,i].T


        # create interpolating function
        x_coords = np.linspace(-dx*.5, dx*.5, px_now.shape[1])
        y_coords = np.linspace(-dy*.5, dy*.5, px_now.shape[0])

        p_function_x = RegularGridInterpolator((x_coords, y_coords), px_now.T)
        p_function_y = RegularGridInterpolator((x_coords, y_coords), py_now.T)

        
        # calculate poynting values
        x_values = p_function_x(p_coord_list)
        y_values = p_function_y(p_coord_list)

        # calculate norms
        p_vectornorm_list = np.sqrt( np.square(x_values) + np.square(y_values) )
        
        # save norms
        p_vectornorm_listlist.append(p_vectornorm_list)

    
    # mean norms along time axis
    p_vectornorm_meanlist = np.mean(p_vectornorm_listlist, axis=0)
    
    # maximum norm of vectors for scaling purposes
    p_vector_mean_maxnorm = np.max(p_vectornorm_meanlist)
    print('\t\t-> <||s||>_max = {0}'.format(p_vector_maxnorm))

    
    
    ########################################
    # 7. plotting
    ########################################


    print('\t- plotting')

    fig, ax = plt.subplots()

    extent=np.array([-dx,dx,-dy,dy])*.5

    ax.imshow(
        data_eps,
        cmap='binary',
        extent=extent
        )

    im = ax.imshow(
        field_z_absmean,
        cmap='viridis',
        alpha=0.9,
        extent=extent,
        )
    
    norm_cmap = plt.get_cmap('Reds')
    
    for c, p_mean_norm in zip(p_coord_list, p_vectornorm_meanlist):
        colorindex = p_mean_norm / p_vector_mean_maxnorm
        color = norm_cmap(colorindex)
        
        ax.plot(
            *c,
            color=color,
            marker='o',
            alpha=0.5,
            markersize=1.,
            markeredgecolor='none'
            )
    
    ax.set_aspect('equal')
    ax.set_axis_off()
    
    fig.savefig(OUTPUTDIR + 'meanpoynting_pattern.png', dpi=300)
    plt.close(fig)
    
    
    print('\t- plotting curve on border')
    
    fig, ax = plt.subplots(figsize=(3*N_CAVITIES,3))
    
    ax.plot(p_s_list, p_vectornorm_meanlist, zorder=1)
    
    for x in range(N_CAVITIES):
        ax.axvline(
            x,
            zorder=2, 
            color='black'
            )
    
    ax.set_xlim(0, N_CAVITIES)
    ax.set_ylim(0, p_vector_mean_maxnorm * 1.05)
    
    ax.set_xlabel('$s$')
    ax.set_ylabel('mean norm of poynting vector')
    
    fig.savefig(OUTPUTDIR + 'meanpoynting_border.png')
    
    ########################################
    # 6. all done!
    ########################################
    
    print('DONE!')
    
    return


if __name__ == '__main__':
    main()