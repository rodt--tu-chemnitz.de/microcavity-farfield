#!/usr/bin/env bash
set -e
export LC_NUMERIC="en_US.UTF-8" # needs to be set so 0,1 (ger) -> 0.1 (en) are given/expected

# make folder for results
scriptdir="$(dirname $0)"

outputdir="$scriptdir/results"
outputdir_csv="$outputdir/csv"
outputdir_params="$outputdir/params"
outputdir_logs="$outputdir/logs"
outputdir_farfield="$outputdir/farfield"
outputdir_fft="$outputdir/fft"
outputdir_mode="$outputdir/mode"
outputdir_dimensions="$outputdir/dimensions"

outputdir_pattern="$outputdir/pattern"
outputdir_pattern_mean="$outputdir_pattern/mean"
outputdir_pattern_start="$outputdir_pattern/start"
outputdir_pattern_end="$outputdir_pattern/end"

outputdir_husimi="$outputdir/husimi"
outputdir_husimi_inner_inc="$outputdir_husimi/inner_inc"
outputdir_husimi_inner_em="$outputdir_husimi/inner_em"
outputdir_husimi_outer_inc="$outputdir_husimi/outer_inc"
outputdir_husimi_outer_em="$outputdir_husimi/outer_em"

outputdir_husimigifs="$outputdir/husimigif"
outputdir_husimigifs_inner_inc="$outputdir_husimigifs/inner_inc"
outputdir_husimigifs_inner_em="$outputdir_husimigifs/inner_em"
outputdir_husimigifs_outer_inc="$outputdir_husimigifs/outer_inc"
outputdir_husimigifs_outer_em="$outputdir_husimigifs/outer_em"

outputdir_poynting="$outputdir/poynting"
outputdir_poynting_border="$outputdir_poynting/border"
outputdir_poynting_pattern="$outputdir_poynting/pattern"
outputdir_poynting_gif="$outputdir_poynting/gif"

mkdir -p $outputdir $outputdir_csv $outputdir_params $outputdir_logs $outputdir_farfield $outputdir_fft $outputdir_mode $outputdir_dimensions
mkdir -p $outputdir_pattern $outputdir_pattern_mean $outputdir_pattern_start $outputdir_pattern_end
mkdir -p $outputdir_husimi $outputdir_husimi_inner_inc $outputdir_husimi_inner_em $outputdir_husimi_outer_inc $outputdir_husimi_outer_em
mkdir -p $outputdir_husimigifs $outputdir_husimigifs_inner_inc $outputdir_husimigifs_inner_em $outputdir_husimigifs_outer_inc $outputdir_husimigifs_outer_em
mkdir -p $outputdir_poynting $outputdir_poynting_border $outputdir_poynting_pattern $outputdir_poynting_gif


# directory of calculations
outputdir_calculations="output"


# distances to check
# dist_list=$(LANG=en seq -f '%.2f' 0 0.01 1 )
inputfilename="$scriptdir/resonances_log.csv"
readarray -t data < "$inputfilename"
# check number of frequencies
n_lines="${#data[*]}"
n_lines=$(($n_lines-1)) # skip header
printf "%.i frequencies found\n" $n_lines


for i in $(seq 1 $n_lines); do
    printf "performing calculations for frequency %.i/%.i...\n" $i $n_lines
    line=( $( printf "${data[$i]}" | tr ", " " ") )
    # printf "line = ${line[*]}\n"


    freq=${line[0]}
    freq_rounded=$( printf "%.6f" $freq )

    printf "\tfreq = $freq_rounded\n"


    outputname="f=$freq_rounded"

    args="--ncavities=1 --fcen=$freq_rounded --df=0.01 --calctime=1e3 --deform=0.4 --resolution=150"

    # execute python code
    printf "\t- calculating...\n"
    bash execall.sh $args # > /dev/null 2>&1 # suppress output and error output


    # save results
    printf "\t- saving results\n"

    # documentation of process
    mv "$outputdir_calculations/log.log" "$outputdir_logs/log_$outputname.log"
    mv "$outputdir_calculations/resonances_log.csv" "$outputdir_csv/resonances_$outputname.csv"
    mv "$outputdir_calculations/params_used.json" "$outputdir_params/params_$outputname.json"
    mv "$outputdir_calculations/cell_dimensions.dat" "$outputdir_dimensions/dimensions_$outputname.dat"

    # mode patterns
    mv "$outputdir_calculations/pattern_start.png" "$outputdir_pattern_start/pattern_start_$outputname.png"
    mv "$outputdir_calculations/pattern_start.dat" "$outputdir_pattern_start/pattern_start_$outputname.dat"
    mv "$outputdir_calculations/pattern_end.png" "$outputdir_pattern_end/pattern_end_$outputname.png"
    mv "$outputdir_calculations/pattern_end.dat" "$outputdir_pattern_end/pattern_end_$outputname.dat"
    mv "$outputdir_calculations/pattern_mean.png" "$outputdir_pattern_mean/pattern_mean_$outputname.png"
    mv "$outputdir_calculations/pattern_mean.dat" "$outputdir_pattern_mean/pattern_mean_$outputname.dat"

    # mode gif
    mv "$outputdir_calculations/mode.gif" "$outputdir_mode/mode_$outputname.gif"

    # husimi results
    mv "$outputdir_calculations/meanhusimi_inner_inc.png" "$outputdir_husimi_inner_inc/meanhusimi_inner_inc_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_inner_em.png" "$outputdir_husimi_inner_em/meanhusimi_inner_em_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_outer_inc.png" "$outputdir_husimi_outer_inc/meanhusimi_outer_inc_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_outer_em.png" "$outputdir_husimi_outer_em/meanhusimi_outer_em_$outputname.png"

    # husimi gifs
    mv "$outputdir_calculations/husimi_inner_inc.gif" "$outputdir_husimigifs_inner_inc/husimi_inner_inc_$outputname.gif"
    mv "$outputdir_calculations/husimi_inner_em.gif" "$outputdir_husimigifs_inner_em/husimi_inner_em_$outputname.gif"
    mv "$outputdir_calculations/husimi_outer_inc.gif" "$outputdir_husimigifs_outer_inc/husimi_outer_inc_$outputname.gif"
    mv "$outputdir_calculations/husimi_outer_em.gif" "$outputdir_husimigifs_outer_em/husimi_outer_em_$outputname.gif"

    # farfield
    mv "$outputdir_calculations/farfield_polar.png" "$outputdir_farfield/farfield_$outputname.png"
    mv "$outputdir_calculations/field_vectors.dat" "$outputdir_farfield/fields_$outputname.dat"

    mv "$outputdir_calculations/field_analysis_fft.png" "$outputdir_fft/fft_$outputname.png"

    # poynting
    mv "$outputdir_calculations/meanpoynting_border.png" "$outputdir_poynting_border/poynting_border_$outputname.png"
    mv "$outputdir_calculations/meanpoynting_pattern.png" "$outputdir_poynting_pattern/poynting_pattern_$outputname.png"
    mv "$outputdir_calculations/poynting.gif" "$outputdir_poynting_gif/poynting_gif_$outputname.gif"


    # deleting old data
    rm -r output/*
done
