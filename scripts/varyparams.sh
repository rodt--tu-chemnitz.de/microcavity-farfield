set -e

deform_list="0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0"

mkdir -p "output_archive/singlelimacon_varepsilon/collection"

for deform in $deform_list; do
    echo $deform

    sh execall.sh --deform=$deform > output_archive/singlelimacon_varepsilon/log.log

    cp "output/husimi_all.png" "output_archive/singlelimacon_varepsilon/collection/husimi_$deform.png"
    cp "output/pattern_end.png" "output_archive/singlelimacon_varepsilon/collection/pattern_$deform.png"
    mv "output" "output_archive/singlelimacon_varepsilon/$deform"
done

python plot_farfield_comparison.py