#!/usr/bin/env bash
set -e
export LC_NUMERIC="en_US.UTF-8" # needs to be set so 0,1 (ger) -> 0.1 (en) are given/expected


# define input
ncavities=$1
df=$2


# make folder for results
outputdir="scripts/compare_couplinginfluence/results"
outputdir_csv="$outputdir/csv"
outputdir_params="$outputdir/params"
outputdir_logs="$outputdir/logs"
outputdir_farfield="$outputdir/farfield"
outputdir_fft="$outputdir/fft"
outputdir_mode="$outputdir/mode"

outputdir_pattern="$outputdir/pattern"
outputdir_pattern_mean="$outputdir_pattern/mean"
outputdir_pattern_start="$outputdir_pattern/start"
outputdir_pattern_end="$outputdir_pattern/end"

outputdir_husimi="$outputdir/husimi"
outputdir_husimi_inner_inc="$outputdir_husimi/inner_inc"
outputdir_husimi_inner_em="$outputdir_husimi/inner_em"
outputdir_husimi_outer_inc="$outputdir_husimi/outer_inc"
outputdir_husimi_outer_em="$outputdir_husimi/outer_em"

outputdir_husimigifs="$outputdir/husimigif"
outputdir_husimigifs_inner_inc="$outputdir_husimigifs/inner_inc"
outputdir_husimigifs_inner_em="$outputdir_husimigifs/inner_em"
outputdir_husimigifs_outer_inc="$outputdir_husimigifs/outer_inc"
outputdir_husimigifs_outer_em="$outputdir_husimigifs/outer_em"

outputdir_poynting="$outputdir/poynting"
outputdir_poynting_border="$outputdir_poynting/border"
outputdir_poynting_pattern="$outputdir_poynting/pattern"
outputdir_poynting_gif="$outputdir_poynting/gif"

output_husimidir="$outputdir/husimidir"
output_husimidir_pattern="$output_husimidir/pattern"
output_husimidir_husimi_inner_inc="$output_husimidir/husimi_inner_inc"
output_husimidir_husimi_outer_em="$output_husimidir/husimi_outer_em"

mkdir -p $outputdir $outputdir_csv $outputdir_params $outputdir_logs $outputdir_farfield $outputdir_fft $outputdir_mode
mkdir -p $outputdir_pattern $outputdir_pattern_mean $outputdir_pattern_start $outputdir_pattern_end
mkdir -p $outputdir_husimi $outputdir_husimi_inner_inc $outputdir_husimi_inner_em $outputdir_husimi_outer_inc $outputdir_husimi_outer_em
mkdir -p $outputdir_husimigifs $outputdir_husimigifs_inner_inc $outputdir_husimigifs_inner_em $outputdir_husimigifs_outer_inc $outputdir_husimigifs_outer_em
mkdir -p $outputdir_poynting $outputdir_poynting_border $outputdir_poynting_pattern $outputdir_poynting_gif
mkdir -p $output_husimidir $output_husimidir_pattern $output_husimidir_husimi_inner_inc $output_husimidir_husimi_outer_em

outputdir_calculations="output"

# distances to check
# dist_list=$(LANG=en seq -f '%.2f' 0 0.01 1 )
inputfilename="scripts/compare_couplinginfluence/resonant_frequencies_edit.dat"
# readarray -t data < "$inputfilename"
# with parantheses the long string is turned into a list using space as seperator
# distlist=(${data[0]})
distlist=($(seq 0 0.05 2.0))
# freqlist=(${data[1]})


for i in ${!distlist[*]}; do
    distnow=${distlist[i]}
    # freqnow=${freqlist[i]}
    freqnow="1.295"

    # round distance due to floatinf point uncertainties
    distnow_rounded=$(printf "%.3f" "$distnow")

    printf "D/R = %.3f\t->\tf = %.6f\n" "$distnow_rounded" "$freqnow"


    outputname="dist=$distnow_rounded"

    args="--cavitydist=$distnow_rounded --ncavities=$ncavities --fcen=$freqnow --df=$df --calctime=1e3 --deform=0.4 --resolution=75"

    # execute python code
    printf "\t- calculating...\n"
    bash execall.sh $args # > /dev/null 2>&1 # suppress output and error output


    # save results
    printf "\t- saving results\n"

    # documentation of process
    mv  "$outputdir_calculations/log.log" "$outputdir_logs/log_$outputname.log"
    mv  "$outputdir_calculations/resonances_log.csv" "$outputdir_csv/resonances_$outputname.csv"
    mv "$outputdir_calculations/params_used.json" "$outputdir_params/params_$outputname.json"

    # mode patterns
    mv "$outputdir_calculations/pattern_start.png" "$outputdir_pattern_start/pattern_start_$outputname.png"
    mv "$outputdir_calculations/pattern_start.dat" "$outputdir_pattern_start/pattern_start_$outputname.dat"
    mv "$outputdir_calculations/pattern_end.png" "$outputdir_pattern_end/pattern_end_$outputname.png"
    mv "$outputdir_calculations/pattern_end.dat" "$outputdir_pattern_end/pattern_end_$outputname.dat"
    mv "$outputdir_calculations/pattern_mean.png" "$outputdir_pattern_mean/pattern_mean_$outputname.png"
    mv "$outputdir_calculations/pattern_mean.dat" "$outputdir_pattern_mean/pattern_mean_$outputname.dat"

    # mode gif
    mv "$outputdir_calculations/mode.gif" "$outputdir_mode/mode_$outputname.gif"

    # husimi results
    mv "$outputdir_calculations/meanhusimi_inner_inc.png" "$outputdir_husimi_inner_inc/meanhusimi_inner_inc_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_inner_em.png" "$outputdir_husimi_inner_em/meanhusimi_inner_em_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_outer_inc.png" "$outputdir_husimi_outer_inc/meanhusimi_outer_inc_$outputname.png"
    mv "$outputdir_calculations/meanhusimi_outer_em.png" "$outputdir_husimi_outer_em/meanhusimi_outer_em_$outputname.png"

    # husimi gifs
    mv "$outputdir_calculations/husimi_inner_inc.gif" "$outputdir_husimigifs_inner_inc/husimi_inner_inc_$outputname.gif"
    mv "$outputdir_calculations/husimi_inner_em.gif" "$outputdir_husimigifs_inner_em/husimi_inner_em_$outputname.gif"
    mv "$outputdir_calculations/husimi_outer_inc.gif" "$outputdir_husimigifs_outer_inc/husimi_outer_inc_$outputname.gif"
    mv "$outputdir_calculations/husimi_outer_em.gif" "$outputdir_husimigifs_outer_em/husimi_outer_em_$outputname.gif"

    # farfield
    mv "$outputdir_calculations/farfield_polar.png" "$outputdir_farfield/farfield_$outputname.png"
    mv "$outputdir_calculations/field_vectors.dat" "$outputdir_farfield/fields_$outputname.dat"

    mv "$outputdir_calculations/field_analysis_fft.png" "$outputdir_fft/fft_$outputname.png"

    # poynting
    mv "$outputdir_calculations/meanpoynting_border.png" "$outputdir_poynting_border/poynting_border_$outputname.png"
    mv "$outputdir_calculations/meanpoynting_pattern.png" "$outputdir_poynting_pattern/poynting_pattern_$outputname.png"
    mv "$outputdir_calculations/poynting.gif" "$outputdir_poynting_gif/poynting_gif_$outputname.gif"


    # husimi dirs
    mv "$outputdir_calculations/arrow_pattern.png" "$output_husimidir_pattern/arrows_$outputname.png"
    mv "$outputdir_calculations/arrow_husimi_inner_inc.png" "$output_husimidir_husimi_inner_inc/arrow_husimi_inner_inc_$outputname.png"
    mv "$outputdir_calculations/arrow_husimi_outer_em.png" "$output_husimidir_husimi_outer_em/arrow_husimi_outer_em_$outputname.png"

    # deleting old data
    rm -r output/*
done

exit