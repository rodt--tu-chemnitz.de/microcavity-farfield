import sys
sys.path.append('../..')
import plotparams

import numpy as np

import matplotlib.pyplot as plt

def main():
    print('PLOTTING DIRECTIONALITY')
    
    print('- reading data')
    
    savedir = 'results/farfield_directionalities/'
    
    dists, directionalities = np.loadtxt(savedir + 'directionalities.dat')
    
    
    print('- plotting')
    fig, ax = plt.subplots()
    
    ax.plot(dists, directionalities, zorder=2, color='black')
    
    ax.axhline(0, color='black', linestyle='dashed', zorder=1, linewidth=1.)
    
    ax.set_xlabel('$D/R$')
    ax.set_ylabel('directionality')
    
    fig.savefig(savedir + 'directionalities.png')
    

if __name__ == '__main__':
    main()