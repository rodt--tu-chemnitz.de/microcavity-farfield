import os
import sys
sys.path.append('../..')

import plotparams

import numpy as np
from scipy.integrate import simpson, trapezoid

import matplotlib.pyplot as plt

def main():
    print('CALCULATING DIRECTIONALITY')
    
    # create directory, where everything is to be saved
    savedir = 'results/farfield_directionalities/'
    try:
        os.makedirs(savedir)
        print('- created directory')
    except FileExistsError:
        print('- directory already exists')
        pass
    
    
    print('- checking available files')
    datadir = 'results/farfield/'
    files_availble = sorted(
        [f for f in os.listdir(datadir) if f[-4:] == '.dat']
        )
    
    
    
    print('- setting up general values')
    
    # integrating function
    integrate = trapezoid
    
    # angle at which spectrum is shifted
    angle_cut = 1.5 * np.pi
    
    # relevant angles
    center_angle_front = 0.
    center_angle_back = np.pi
    delta_angle = 30 * np.pi / 180. # integration over +- 30° from center
    

    print('- evaluating data')
    data_list = []
    dist_list = []
    for fname in files_availble:
        print(fname)
        dist = float( fname[:-4].split('_')[1].split('=')[1] )
        dist_list.append(dist)
        
        data = np.loadtxt(datadir + fname, dtype=complex)
        angles, Ex, Ey, Ez, Hx, Hy, Hz = data
        angles = np.real(angles)
        Ex, Ey, Ez = np.conj([Ex, Ey, Ez])
        
        # all three components must be calculated, so the total intensity Pr
        # can be calculated for any type of polarization (TM vs TE)
        Px = np.real( Ey * Hz - Ez * Hy )
        Py = np.real( Ez * Hx - Hx * Hz )
        Pz = np.real( Ex * Hy - Ey * Hx )
        Pr = np.sqrt( np.square(Px) + np.square(Py) + np.square(Pz) )
        
        # Pr = np.abs(Ez)
        Pr = Pr / np.max(Pr)
        

        
        # shifting spectrum
        angle_boollist = angles > angle_cut
        
        angles_left = angles[np.logical_not(angle_boollist)]
        angles_right = angles[angle_boollist]
        
        Pr_left = Pr[np.logical_not(angle_boollist)]
        Pr_right = Pr[angle_boollist]
        
        angles_new = np.concatenate((angles_right - np.pi * 2., angles_left))
        Pr_new = np.concatenate((Pr_right, Pr_left))
        
        
        
        # fields at angles
        front_boollist = (center_angle_front - delta_angle < angles_new) & (angles_new < center_angle_front + delta_angle)
        front_angles = angles_new[front_boollist]
        front_Pr = Pr_new[front_boollist]
        
        back_boollist = (center_angle_back - delta_angle < angles_new) & (angles_new < center_angle_back + delta_angle)
        back_angles = angles_new[back_boollist]
        back_Pr = Pr_new[back_boollist]

        
        # calculate total intensities in direction
        
        intensity_front = integrate(y=front_Pr, x=front_angles)
        intensity_back = integrate(y=back_Pr, x=back_angles)
        
        total_angles = np.concatenate((angles, [angles[0]+np.pi*2.]))
        total_Pr = np.concatenate((Pr, [Pr[0]]))
        intensity_total = integrate(y=total_Pr, x=total_angles)
        # intensity_total = intensity_front + intensity_back
        
        directionality = (intensity_front-intensity_back) / intensity_total
        print('\tfront = {0:.6f}, back = {1:.6f}, total = {2:.6f}'.format(intensity_front, intensity_back, intensity_total))
        print('\tdirectionality : {0:.6f}'.format(directionality))
        
        # save result
        data_list.append([dist, directionality])
        
        
        if True:
            fig, ax = plt.subplots(
                subplot_kw={'projection' : 'polar'}
                )
            
            ax.plot(angles, Pr, color='black', label='farfield')
            # ax.plot(angles_new, Pr_new, linestyle='dashed')
            
            ax.plot(front_angles, front_Pr, linestyle=':', label='front')
            ax.plot(back_angles, back_Pr, linestyle=':', label='back')
            
            ax.set_ylim(0,1)
            ax.set_yticks([0,0.5,1])
            
            ax.legend(framealpha=1., loc='lower center')
            ax.set_title(
                '$\mathrm{dir} ' + '= ({0:.2f} - {1:.2f})/{2:.2f} = {3:.2f}$'.format(
                    intensity_front, intensity_back, intensity_total, directionality
                    )
                )
            
            fig.savefig(savedir + 'farfield_parts_dist={0:.3f}.png'.format(dist))
            plt.close(fig)
            
    
    print('- saving results')
    np.savetxt(savedir + 'directionalities.dat', np.transpose(data_list))
    
    print('Done!')
        
    


if __name__ == '__main__':
    main()