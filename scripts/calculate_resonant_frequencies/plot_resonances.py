'''
This programm reads the csv tables and plots the resonances
'''

import os
import sys
sys.path.append('../../') 

import pandas as pd
import numpy as np

import plotparams
import matplotlib.pyplot as plt

def main():
    print('PLOTTING FOUND RESONANCES')
    
    ###########################################
    # setup
    ###########################################

    # read available files

    csvdir = 'results/csv/'
    savedir = 'results/results/'

    fnames = sorted(
        os.listdir(csvdir),
        key=extract_dist # sort by intercavity dist
        )



    ###########################################
    # reading data
    ###########################################
    
    print('reading data...')

    # select frequencies of interest
    fi = 0.725 # looking around that frequency
    fi_maxdiff = 0.005 # max tolerance
    fi_list = [] # list, where interesting frequencies lie

    # initialize
    dist_list = []
    f_real_listlist = []
    Q_listlist = []

    # initialize to find max values for each
    Qmin = 1e6
    Qmax = 0.

    for fname in fnames:
        dist = extract_dist(fname)
        dist_list.append(dist)
        
        df = pd.read_csv(csvdir + fname, sep=', ', engine='python')

        f_now = df['frequency'].values
        f_real_listlist.append(f_now)

        Q_factors_now = np.abs(df['Q'].values)
        Q_listlist.append(Q_factors_now)


        # grab relevant frequency
        fi_difflist = np.abs(f_now - fi)
        index = np.argmin(fi_difflist)
        fi_now = f_now[index]
        fi_list.append(fi_now)



        # get maxmum Q
        Qmax_now = np.max(Q_factors_now)
        if Qmax_now > Qmax:
            Qmax = Qmax_now
        
        # get minimum Q
        Qmin_now = np.min(Q_factors_now)
        if Qmin_now < Qmin:
            Qmin = Qmin_now
 
    print('saving resonances near f={0:.3f}...'.format(fi))

    # create savedirectory folder
    try:
        os.mkdir(savedir)
    # if it already exists, do nothing
    except FileExistsError:
        pass
    
    np.savetxt(
        savedir + 'resonant_frequencies_raw.dat',
        [dist_list, fi_list]
        )


    ###########################################
    # plotting
    ###########################################

    print('plotting...')

    colormap = plt.get_cmap('binary')
    
    Qmin_log = np.log(Qmin)
    Qmax_log = np.log(Qmax)
    Q_log_delta = Qmax_log - Qmin_log

    fig, ax = plt.subplots()


    for dist, Q_list, f_real_list in zip(dist_list, Q_listlist, f_real_listlist):
        for Q, f in zip(Q_list, f_real_list):
            colorindex = (np.log(Q) - Qmin_log) / Q_log_delta
            color = colormap(colorindex)

            ax.plot(
                dist, 
                f, 
                color=color,
                marker='x',
                # alpha=colorindex,
                markeredgecolor=None,
                zorder=1
                )

    ficolor = 'red'
    ax.plot(dist_list, fi_list, color=ficolor, zorder=2)
    ax.axhline(fi, color='black', linestyle='--', zorder=0)

    dy = 0.025
    ycen = fi
    ax.set_ylim(ycen-dy, ycen+dy)

    ax.set_xlabel('$D/R$')
    ax.set_ylabel('$f$')

    fig.savefig(savedir + 'resonances.png')
    
    print('done!')



def extract_dist(s:str) -> float:
    '''
    extracts the distance from the filenames
    '''
    return float(s[:-4].split('=')[1])

if __name__ == '__main__':
    main()