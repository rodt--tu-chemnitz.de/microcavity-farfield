#!/usr/bin/env bash
set -e
export LC_NUMERIC="en_US.UTF-8" # needs to be set so 0,1 (ger) -> 0.1 (en) are given/expected

ncavities=$1
printf "CALCULATING RESONANT FREQUENCIES FOR %i CAVITIES\n\n" "$ncavities"

# make folder for results
outputdir="scripts/calculate_resonant_frequencies/results"
outputdir_csv="$outputdir/csv"
outputdir_pattern_start="$outputdir/pattern/start"
outputdir_pattern_end="$outputdir/pattern/end"
outputdir_params="$outputdir/params"
outputdir_logs="$outputdir/logs"
outputdir_fft="$outputdir/fft"
mkdir -p $outputdir $outputdir_csv $outputdir_pattern_start $outputdir_pattern_end $outputdir_params $outputdir_logs $outputdir_fft

outputdir_calculations="output"

# distances to check
dist_list=$(LANG=en seq -f '%.2f' 0 0.01 2 )


for dist in $dist_list; do
    printf "D/R = %.2f\n" $dist

    outputname="dist=$dist"

    args="--cavitydist=$dist --ncavities=$ncavities --fcen=1.00 --df=1.0 --calctime=1e3 --deform=0.4 --resolution=100"

    # execute python code
    printf "\t- calculating...\n"
    python3 main.py $args # > "$outputdir_logs/$outputname.log" 2>&1
    python3 plot_patterns.py # >> "$outputdir_logs/$outputname.log" 2>&1

    mv "main-field_at_harminv.h5" "$outputdir_calculations/main-field_at_harminv.h5"
    python3 analyze_field_at_harminv.py # >> "$outputdir_logs/$outputname.log" 2>&1

    # save results
    printf "\t- saving results\n"
    cat "$outputdir_logs/$outputname.log" | grep 'harminv2:' | cut -c 12- > "$outputdir_csv/$outputname.csv"
    mv "$outputdir_calculations/params_used.json" "$outputdir_params/params_$outputname.json"
    mv "$outputdir_calculations/pattern_start.png" "$outputdir_pattern_start/pattern_start_$outputname.png"
    mv "$outputdir_calculations/pattern_end.png" "$outputdir_pattern_start/pattern_start_$outputname.png"
    mv "$outputdir_calculations/field_analysis_fft.png" "$outputdir_fft/fft_$outputname.png"
done
