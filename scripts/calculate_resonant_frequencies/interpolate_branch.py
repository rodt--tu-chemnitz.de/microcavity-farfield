import os
import sys
sys.path.append('../..') 

import pandas as pd
import numpy as np
from scipy import interpolate

import plotparams
import matplotlib.pyplot as plt


resultsdir = 'results/results/'

# interpolate
try:
    data_raw = np.loadtxt(resultsdir + 'resonant_frequencies_raw.dat')

    resonant_frequencies = interpolate.interp1d(
        *data_raw,
        kind='cubic',
        assume_sorted=True,
        bounds_error=True
        )

    resonant_frequencies_lininterpol = interpolate.interp1d(
        *data_raw,
        kind='linear',
        assume_sorted=True,
        bounds_error=True
        )
except FileNotFoundError:
    print('WARNING: raw resonance data not found')

try:
    data_edit = np.loadtxt(resultsdir + 'resonant_frequencies_edit.dat')
except FileNotFoundError:
    print('WARNING: edited resonance data not found')


def plot_branch():
    # data_raw = np.loadtxt('results/resonant_frequencies_raw.dat')

    fig, ax = plt.subplots(figsize=(10,3))

    # ax.plot(*data_raw)

    ax.plot(
        *data_raw, 
        color='black', 
        marker='D', 
        linestyle='-',
        markersize=4.,
        label='raw data'
        )
    
    ax.plot(
        *data_edit, 
        color='red', 
        marker='x', 
        linestyle='-',
        markersize=4.,
        label='raw data'
        )
    
    # show interpolations
    if False:
        xlist = np.linspace(data_raw[0,0],data_raw[0,-1],1001)

        ax.plot(
            xlist,resonant_frequencies(xlist), 
            color='tab:blue', 
            label='interpolation (cubic)'
            )
        
        ax.plot(
            xlist,resonant_frequencies_lininterpol(xlist), 
            color='tab:orange', 
            label='interpolation (linear)',
            linestyle='--'
            )

    ax.legend()

    ax.set_xlabel('$D/R$')
    ax.set_ylabel('$f$')

    fig.savefig(resultsdir + 'branch.png')



if __name__ == '__main__':
    plot_branch()