import numpy as np
import h5py
import os


import plotparams
plt = plotparams.matplotlib.pyplot

import params as pr
from limacon_class import Limacon

from scipy.interpolate import RegularGridInterpolator, interp1d

PLOTTING = True

def main():
    DATADIR = 'output/'
    OUTPUTDIR = DATADIR + 'borderfunctions_period/'
    PLOTDIR_PATTERN = OUTPUTDIR + 'fieldplots/'
    PLOTDIR_CURVES = OUTPUTDIR + 'curves/'

    pr.trycreatedir(OUTPUTDIR)

    # plot-directories are only created if needed
    if PLOTTING:
        pr.trycreatedir(PLOTDIR_PATTERN)
        pr.trycreatedir(PLOTDIR_CURVES)


    ###################################################
    # calculating data
    ###################################################

    borderfunc_data = []
    profile_list = []


    #--------------------------------------------------
    # 1 calculating data
    #--------------------------------------------------

    print('\t- load params')
    
    params = pr.load_useparams(fname=DATADIR + 'params_used.json')


    N_CAVITIES = params['n_cavities']
    
    # geometry params
    RADIUS = params['radius']
    DEFORM = params['deform']
    N_CAVITIES = params['n_cavities']
    DPML_ONE = params['dpml']
    
    # get centerlist of limacons

    limacon_centerdist = pr.get_limacon_centerdist(params=params)
    center_ylist = np.array([limacon_centerdist * i for i in range(N_CAVITIES)])
    center_ylist = center_ylist - np.mean(center_ylist)
    center_ylist = np.flip(center_ylist) # flip list so highest y comes first
    
    
    # determine cell size

    # old way by calculating dimensions
    # dx, dy = pr.get_cell_dimensions(params=params, withpml=False)
    
    # new way by reading dimensions
    dx, dy, _, _ = np.loadtxt(DATADIR + 'cell_dimensions.dat')
    
    # remove thickness of PMLs, which are ignored
    dx -= 2. * DPML_ONE
    dy -= 2. * DPML_ONE
    
    print('dx : {0}'.format(dx))
    print('dy : {0}'.format(dy))
    
    

    
    
    print('extracting borderfunction of period')
    
    data_period = h5py.File(DATADIR + 'main-mode_period.h5', 'r')[ params['component'] ]
    n_timesteps = data_period.shape[2]
    print(data_period.shape)
    
    print('-> {0} timesteps'.format(n_timesteps))

    for timestep in range(n_timesteps):
        print('timestep {0}/{1}'.format(timestep+1, n_timesteps))

        #--------------------------------------------------
        # 1 get data
        #--------------------------------------------------
        
        # grabbing correct timestep
        pattern_now = data_period[:,:,timestep].T

        
        # normalization
        # pattern_now = pattern_now / np.max(np.abs(pattern_now))
        

        #--------------------------------------------------
        # 2 extracting function
        #--------------------------------------------------
        
        # create interpolating function
        print('\t- create interpolating function')
        x_coords = np.linspace(-dx*.5, dx*.5, pattern_now.shape[1])
        y_coords = np.linspace(-dy*.5, dy*.5, pattern_now.shape[0])
        # xg, yg = np.meshgrid(x_coords, y_coords)
        field_function = RegularGridInterpolator(
            (x_coords, y_coords), pattern_now.T,
            bounds_error=False,
            fill_value=0.
            )
        


        #--------------------------------------------------
        # 3 extract field
        #--------------------------------------------------
        
        
        print('\t- extract field')

        # for calculation of derivative
        # distance between boundary functions
        delta_r = 0.01

        value_listlist = []
        # structure of listlist
        # cavity1 (inner, middle, outer), cavity2 (inner, middle, outer), ...
        circle_listlist = []
        
        # make radius smaller so the expected limacon border
        # lies on the found limacon border
        radius_factor = 1.0


        l = Limacon(
            RADIUS*radius_factor, 
            DEFORM, 
            None,
            n_edgepoints=200,
            phi_of_s_path='notebooks/phi_of_s.csv'
            )
        

        # read interpolated data
        coords = l.equidist_s_verticelist
        normals = l.normal_vector(l.equidist_s_philist)
        for y in center_ylist:
            for delta in np.array([-1,0,1]) * delta_r:
                coords_now = coords + normals * delta + np.array([0,y])
                disc_values = field_function(coords_now)
                
                circle_listlist.append(coords_now)
                value_listlist.append(disc_values)



        print('\t- calculate derivatives')
        
        
        value_listlist = np.array(value_listlist)
        s_list = np.arange(value_listlist[0].size) / value_listlist[0].size
        
        alldata_list = []
        for i in range(N_CAVITIES):
            s_list_now = s_list + i
            
            index = i * 3 # for 3 arrays of data -> inner, middle, outer
            values_inner = value_listlist[index]
            values_middle = value_listlist[index+1]
            values_outer = value_listlist[index+2]
        
            derivative_inner = ( values_middle - values_inner ) / delta_r
            derivative_outer = ( values_outer - values_middle ) / delta_r
            
            # saving
            data_now = [s_list_now, values_middle, derivative_inner, derivative_outer]
            alldata_list.append(data_now)
        
        if N_CAVITIES > 1:
            alldata_list = np.concatenate(alldata_list, axis=1) # append all data
        else:
            alldata_list = np.array(alldata_list[0])

        
        
        print('\t- saving results')


        np.savetxt(
            OUTPUTDIR + 'boundaryfunc_{0:03d}.dat'.format(timestep),
            alldata_list
            )
        
        borderfunc_data.append(alldata_list)



        ###############################################################################
        # plots
        ###############################################################################
        
        if PLOTTING:
            print('\t-> plotting field')



            extent = [-dx*.5, dx*.5, -dy*.5, dy*.5]

            absmax = np.max(np.abs(pattern_now))

            fig, ax = plt.subplots()

            im = ax.imshow(
                pattern_now,
                extent=extent,
                cmap='coolwarm',
                vmin=-absmax,
                vmax=+absmax,
                origin='lower'
                )
            
            ax.plot(*np.transpose(coords), color='black', ls='--')

            fig.colorbar(im, label='$E_z$')

            fig.savefig(PLOTDIR_PATTERN + 'boundaryfunc_pattern_{0:03d}.png'.format(timestep))
            plt.close(fig)



            print('\t-> plotting resulting functions')
            
            fig, axes, = plt.subplots(
                nrows=2,
                sharex=True,
                figsize=(6*N_CAVITIES,3)
                )

            ax_lines, ax_derivatives = axes

            s_list_plot = alldata_list[0]



            for i in range(N_CAVITIES):
                s_list_now = s_list+i
                field_inner, field_border, field_outer = value_listlist[i*3:i*3+3]

                ax_lines.plot(
                    s_list_now,
                    field_border,
                    color='black',
                    linestyle='-',
                    zorder=2
                    )
                
                ax_lines.fill_between(
                    s_list_now,
                    field_inner,
                    field_outer,
                    color='gray',
                    linestyle='-',
                    zorder=1
                    )
            

            
            # compare to new points

            # plotting derivative
            for y_list, label in zip(alldata_list[2:], ['inner','outer']):
                ax_derivatives.plot(
                    s_list_plot,
                    y_list,
                    label=label
                    )
            
            # options
            ax_derivatives.set_xlabel(r'$s$')
            
            ax_lines.set_ylabel(r'$E_z$')
            ax_derivatives.set_ylabel(r'$\partial E_z / \partial r$')
            
            ax_derivatives.legend()
            
            for ax in axes:
                # show 0
                ax.axhline(
                    0,
                    color='black',
                    linestyle='--',
                    zorder=0,
                    linewidth=1.
                    )
                
                # set
                ax.set_xlim(0, N_CAVITIES)
                
            # show seperation of microcavities
            if N_CAVITIES > 1:
                for ax in axes:
                    for x in range(N_CAVITIES-1):
                        ax.axvline(x+1,
                            color='black',
                            linestyle='-',
                            zorder=3,
                            linewidth=1.
                            )                

            
            fig.savefig(PLOTDIR_CURVES + 'boundaryfunc_results_{0:03d}.png'.format(timestep),dpi=300, bbox_inches='tight')
            plt.close(fig)
        
        # return
        


if __name__ == '__main__':
    main()