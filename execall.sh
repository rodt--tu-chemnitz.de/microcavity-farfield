#!/usr/bin/env bash
set -e

# commands:
# '--deform=',              # deform value of limacons
# '--ncavities=',           # number of limacons
# '--cavitydist=',          # distance between limacons
# '--fcen=',                # frequency
# '--df=',                  # frequency width of source
# '--calctime=',            # calculation time after sources
# '--resolution=',          # resolution of cell
# '--saveallcomponents=',   # save all field components for further study
# '--sourcetype=',          # distinguish between normal point source and planewave
# '--symmetry=',            # symmetry of cell along y axis, is either "symmetric" or "antisymmetric"
# '--outputdir='            # directory, where all results are to be saved


# grab outputfolder option
# while true; do
#     echo "arg $1"
#     case "$1" in
#         --outputdir* )
#             echo "found one with $1"
#             outputfolder=$(echo $1 | sed -e 's:[^=]*=::')
#             break
#             ;;
#     esac
#     shift
# done

# # check if outputfolder was specified
# if [ -z ${outputfolder+x} ]
# then
#     echo "outputfolder is unset, using default"
#     outputfolder="output"
# fi

# echo "outputfolder is set to '$outputfolder'"

# create output folder
outputfolder=output
mkdir -p $outputfolder

# execute meep code
python3 main.py $@ | tee $outputfolder/log.log
# mv params_used.json $outputfolder # so it is clear which parameters were use in the calculation

# if you want to save the resonances
cat "$outputfolder/log.log" | grep 'harminv2:' | cut -c 12- > $outputfolder/resonances_log.csv

# read used cell_dimensions
cat "$outputfolder/log.log" | grep "Computational cell is" | grep -oE "[0-9]+\.?[0-9]*" > "$outputfolder/cell_dimensions.dat"       

# plot field patterns
echo
echo
python3 plot_farfield.py
echo
echo
python3 plot_patterns.py



# crate GIF of mode
mv "main-eps-000000000.h5" "$outputfolder/main-eps-000000000.h5"
mv "main-mode_period.h5" "$outputfolder/main-mode_period.h5"

mkdir -p TEMP
rm -f TEMP/*   # --force so the script doesn't abort if nothing in TEMP exists
python3 plot_timesteps.py
convert -rotate 90 TEMP/img_*.png output/mode.gif
convert -rotate 90 TEMP/imgmagn_*.png output/mode_magnified.gif
 


# calculate Husimi
python3 extract_borderfunctions.py
# python3 calculate_radial_quantumnumber.py

# calculating husimi functions and plots them for each timestep
python3 calculate_husimi.py
python3 calculate_meanhusimi.py


# create gifs of husimi functions
husimigif_savedir="$outputfolder/husimi/gifs/"
mkdir -p $husimigif_savedir

printf "\n\nCREATING HUSIMI GIFS\n\t- inner incident\n"
convert "$outputfolder/husimi/samenorm/husimi_1_inc/husimi_1_inc_*.png" "$husimigif_savedir/husimi_1_inc.gif"
printf "\t- inner emerging\n"
convert "$outputfolder/husimi/samenorm/husimi_1_em/husimi_1_em_*.png" "$husimigif_savedir/husimi_1_em.gif"
printf "\t- outer incident\n"
convert "$outputfolder/husimi/samenorm/husimi_0_inc/husimi_0_inc_*.png" "$husimigif_savedir/husimi_0_inc.gif"
printf "\t- outer emerging\n"
convert "$outputfolder/husimi/samenorm/husimi_0_em/husimi_0_em_*.png" "$husimigif_savedir/husimi_0_em.gif"



printf "\nPLOTTING PYONTING VECTORS\n"
rm -f TEMP/*
python3 calculate_radial_quantumnumber.py
python3 plot_poynting.py
convert -rotate 90 "output/meanpoynting_pattern.png" "output/meanpoynting_pattern.png"

printf "\t- creating GIF\n"
convert -delay 20 -rotate 90 "TEMP/*.png" "output/poynting.gif"


printf "\nANALYZING HUSIMI FUNCTIONS\n"
python3 husimi_directions.py
convert -rotate 90 "$outputfolder/arrow_pattern.png" "$outputfolder/arrow_pattern.png"


printf "\nEXECALL DONE!\n"

exit
