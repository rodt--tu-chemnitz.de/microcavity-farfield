#!/usr/bin/env bash

# This script only executes the scripts necessary for the husimi function.
# It can be used to discern the influence of variations in calculations on the exact husimi functions



set -e



outputfolder="output"

# calculate Husimi
mkdir -p "$outputfolder/borderfunctions_period"
python3 extract_borderfunctions.py
python3 calculate_husimi.py

# create gifs of husimi functions
printf "\n\nCREATING HUSIMI GIFS\n\t- inner incident\n"
convert "$outputfolder/borderfunctions_period/husimi_inner_inc_*.png" "$outputfolder/husimi_inner_inc.gif"
printf "\t- inner emerging\n"
convert "$outputfolder/borderfunctions_period/husimi_inner_em_*.png" "$outputfolder/husimi_inner_em.gif"
printf "\t- outer incident\n"
convert "$outputfolder/borderfunctions_period/husimi_outer_inc_*.png" "$outputfolder/husimi_outer_inc.gif"
printf "\t- outer emerging\n"
convert "$outputfolder/borderfunctions_period/husimi_outer_em_*.png" "$outputfolder/husimi_outer_em.gif"



printf "\nPLOTTING PYONTING VECTORS\n"
rm -f TEMP/*
python3 calculate_radial_quantumnumber.py
python3 plot_poynting.py
convert -rotate 90 "output/meanpoynting_pattern.png" "output/meanpoynting_pattern.png"

printf "\t- creating GIF\n"
convert -delay 20 -rotate 90 "TEMP/*.png" "output/poynting.gif"


printf "\nANALYZING HUSIMI FUNCTIONS"
python3 husimi_directions.py
convert -rotate 90 "$outputfolder/arrow_pattern.png" "$outputfolder/arrow_pattern.png"


printf "\nEXECALL DONE!\n"

exit
