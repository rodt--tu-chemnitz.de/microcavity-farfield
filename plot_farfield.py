import matplotlib.pyplot as plt
import numpy as np
import params as pr

def main():
    print('PLOTTING FAR FIELD')
    
    print('- loading data')
    
    params = pr.load_useparams('output/params_used.json')

    OUTPUTDIR = params["outputfolder"]
    
    data = np.loadtxt(
        OUTPUTDIR + 'field_vectors.dat',
        dtype=complex
        )
    angles, Ex, Ey, Ez, Hx, Hy, Hz = data
    angles = np.real(angles)
    Ex, Ey, Ez = np.conj([Ex, Ey, Ez])
    
    # all three components must be calculated, so the total intensity Pr
    # can be calculated for any type of polarization (TM vs TE)
    Px = np.real( Ey * Hz - Ez * Hy )
    Py = np.real( Ez * Hx - Hx * Hz )
    Pz = np.real( Ex * Hy - Ey * Hx )
    Pr = np.sqrt( np.square(Px) + np.square(Py) + np.square(Pz) )
    
    # Pr = np.abs(Ez)
    Pr = Pr / np.max(Pr)


    
    print('- plotting polar representation')
    
    marker = None

    fig, ax = plt.subplots(
        subplot_kw={'projection' : 'polar'}
        )

    ax.plot(
        angles,
        Pr,
        marker=marker
        )
    
    # plotting the far field in the direction of theta, for comparison with the Pr intensity
    # currently disabled, as it should not have a big impact. if it would have, the farfield
    # transformation wouldn't make any sense
    if False:
        newscale_list = []
        for theta, Px_now, Py_now in zip(angles, Px, Py):
            vec = np.array([Px_now * np.cos(theta), Py_now * np.sin(theta)])
            Pr_now = np.linalg.norm(vec)
            
            newscale_list.append(Pr_now)
        newscale_list = np.array(newscale_list) / np.max(newscale_list)

        ax.plot(
            angles,
            newscale_list ,
            marker=marker
            )
    
    
    if False: # can show Ez on borders if required
        Ezabs = np.abs(Ez)
        ax.plot(
            angles,
            Ezabs / np.max(Ezabs),
            marker=marker
            )


    ax.set_rticks([0, 0.5, 1.0])

    fig.savefig(
        OUTPUTDIR + 'farfield_polar.png',
        dpi=300,
        bbox_inches='tight'
        )
    plt.close(fig)


    print('- plotting kartesian representation')

    fig, ax = plt.subplots()

    ax.plot(
        angles[:-1] / np.pi,
        Pr[:-1],
        marker=marker
        )

    # ax.set_xlabel(r'$\varphi$ [$\pi$]')
    # ax.set_ylabel(r'farfild intensity [a.u.]')

    fig.savefig(
        OUTPUTDIR + 'farfield_kartesian.png',
        dpi=300,
        bbox_inches='tight'
        )
    plt.close(fig)

    print('- done!')

if __name__ == '__main__':
    main()