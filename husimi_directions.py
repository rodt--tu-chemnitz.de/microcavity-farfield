import sys
sys.path.append('../..')

import params as pr
import plotparams

from limacon_class import Limacon
import meep as mp

from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema


def main():
    print('Plotting all timesteps')

    print('\t- load params')
    
    params = pr.load_useparams(fname='output/params_used.json')
    
    OUTPUTDIR = params["outputfolder"]

    # DPML = params['dpml']
    DPML = 0. # not shown in file
    RESOLUTION = params['resolution']
    N_CAVITIES = params['n_cavities']
    INDEX = params['index']

    RADIUS = params['radius']
    DEFORM = params['deform']
    DIST_NFB = params['dist_nfb']
    PAD = params['pad']



    # dx = dy = (RADIUS + DIST_NFB + PAD + DPML)*2.

    dx, dy = pr.get_cell_dimensions(params=params, withpml=False)


    print('\t- load data')

    eps_filename = 'geometry.dat'
    data_eps = np.loadtxt(OUTPUTDIR + eps_filename)

    field_filename = 'pattern_mean.dat'
    data_field = np.loadtxt(OUTPUTDIR + field_filename)

    
    print('\t- extract field')


    print('\t- constructing limacon coords')
    
    # get centerlist of limacons

    limacon_centerdist = pr.get_limacon_centerdist(params=params)
    center_ylist = np.array([limacon_centerdist * i for i in range(N_CAVITIES)])
    center_ylist = center_ylist - np.mean(center_ylist)

        
    l = Limacon(
        RADIUS*0.99, 
        DEFORM, 
        None,
        n_edgepoints=200
        )
    
    # read interpolated data
    
    border_c_onelimacon = l.equidist_s_verticelist
    border_s_onelimacon = l.equidist_s_slist
    
    border_c = []
    border_s = []
    
    lowest_centerpoint = - limacon_centerdist * ( (N_CAVITIES - 1) // 2 )
    
    for i in range(N_CAVITIES):
        border_c.append( border_c_onelimacon + i * np.array([0, limacon_centerdist]) + np.array([0, lowest_centerpoint]))
        border_s.append( border_s_onelimacon + i )
    
    border_c = np.concatenate(border_c)
    normals_c = np.concatenate( [l.normal_vector(l.equidist_s_philist) for i in range(N_CAVITIES)] )
    border_s = np.concatenate(border_s)
    
    
    ##############################################
    # Analyzing Husimi Function
    ##############################################
    
    # outer emerging
    
    df_outer_em = pd.read_csv(OUTPUTDIR + 'meanhusimi_outer_em.csv', sep=',')
    
    outer_em_p_list = df_outer_em['Unnamed: 0'].values
    
    outer_em_s_list = []
    for s in df_outer_em.keys()[1:]:
        outer_em_s_list.append( float(s) )

    outer_em_s_list = np.array(outer_em_s_list)

    outer_em_husimi_vals = df_outer_em.values[:,1:]
    outer_em_p_index_list, outer_em_s_index_list, outer_em_val_list = find_maxima2d(outer_em_husimi_vals, N_CAVITIES)
    

    outer_em_s_peak_list = outer_em_s_list[outer_em_s_index_list]
    outer_em_p_peak_list = outer_em_p_list[outer_em_p_index_list]
    outer_em_maxval = np.max(outer_em_val_list)
    
    
    # inner incoming
    
    df_inner_inc = pd.read_csv(OUTPUTDIR + 'meanhusimi_inner_inc.csv', sep=',')
    
    inner_inc_p_list = df_inner_inc['Unnamed: 0'].values
    
    inner_inc_s_list = []
    for s in df_inner_inc.keys()[1:]:
        inner_inc_s_list.append( float(s) )

    inner_inc_s_list = np.array(inner_inc_s_list)

    inner_inc_husimi_vals = df_inner_inc.values[:,1:]
    inner_inc_p_index_list, inner_inc_s_index_list, inner_inc_val_list = find_maxima2d(inner_inc_husimi_vals, N_CAVITIES)
    

    inner_inc_s_peak_list = inner_inc_s_list[inner_inc_s_index_list]
    inner_inc_p_peak_list = inner_inc_p_list[inner_inc_p_index_list]
    inner_inc_maxval = np.max(inner_inc_val_list)
    
    
    ##############################################
    # Plotting
    ##############################################

    # initialize
    fig_pattern, ax_pattern = plt.subplots(figsize=(6,6))

    extent=np.array([-dx,dx,-dy,dy])*.5

    ax_pattern.imshow(
        data_eps,
        cmap='gist_gray',
        extent=extent
        )
    ax_pattern.imshow(
        data_field,
        cmap='gist_gray',
        extent=extent,
        alpha=0.8
        )
    
    ##############################################
    # Plotting Husimis
    ##############################################
    
    # outer, emerging
    
    fig_outer_em, ax_outer_em = plt.subplots(
        figsize=(3*N_CAVITIES+2, 3)
        )
    
    im = ax_outer_em.contourf(
        outer_em_s_list, outer_em_p_list, outer_em_husimi_vals
        )
    fig_outer_em.colorbar(im, ax=ax_outer_em, label='$H_1^\mathrm{em}$', pad=0.01)

    
    
    
    # inner, incoming
    
    fig_inner_inc, ax_inner_inc = plt.subplots(
        figsize=(3*N_CAVITIES+2, 3)
        )
    
    im = ax_inner_inc.contourf(
        inner_inc_s_list, inner_inc_p_list, inner_inc_husimi_vals,
        vmin=0
        )
    fig_inner_inc.colorbar(im, ax=ax_inner_inc, label='$H_0^\mathrm{inc}$', pad=0.01)


    ##############################################
    # Joining everything
    ##############################################
    

    # Paint arrows
    arrow_maxlength = 0.75
    arrow_inner_threshold = 0.
    arrow_outer_threshold = 0.
    
    arrowcolor_inner = 'tab:blue'
    arrowcolor_outer = 'tab:green'
    

    # inner, incoming
    for s, p, val in zip(inner_inc_s_peak_list, inner_inc_p_peak_list, inner_inc_val_list):
        
        if val/inner_inc_maxval < arrow_inner_threshold:
            continue
        
        
        # mark loc of arrow
        ax_inner_inc.plot(s,p, color='white', marker='x')
        
        
        # draw arrows
        
        index_on_surface = np.argmin(np.square(border_s - s))
        
        coord = border_c[index_on_surface]
        normal = normals_c[index_on_surface]
        
        direction = normal @ rotmat(p * np.pi * 0.5)
        
        dr = direction * val / inner_inc_maxval * arrow_maxlength
        r0 = coord - dr
        
        # ax.plot(*coord, color='white', marker='o')
        ax_pattern.arrow(
            *r0, *dr,
            color=arrowcolor_inner,
            length_includes_head=True,
            width=0.01,
            zorder=5
            )
    
    
    
    # outer, emitting
    for s, p, val in zip(outer_em_s_peak_list, outer_em_p_peak_list, outer_em_val_list):
        
        if val/outer_em_maxval < arrow_outer_threshold:
            continue
        
        # mark loc of arrow
        
        ax_outer_em.plot(s,p, color='white', marker='x')
        
        
        # draw arrow
        
        index_on_surface = np.argmin(np.square(border_s - s))
        
        coord = border_c[index_on_surface]
        normal = normals_c[index_on_surface]
        
        direction = normal @ rotmat(p * np.pi * 0.5)
        
        r0 = coord
        dr = direction * val / outer_em_maxval * arrow_maxlength
        
        ax_pattern.arrow(
            *r0, *dr,
            color=arrowcolor_outer,
            length_includes_head=True,
            width=0.01,
            zorder=5
            )


    ##############################################
    # Saving plot
    ##############################################
    
    ax_pattern.set_axis_off()

    ax_pattern.tick_params(
        direction='inout'
        )

    ax_pattern.set_xlabel('$x$')
    ax_pattern.set_ylabel('$y$')

    fig_pattern.savefig(OUTPUTDIR + 'arrow_pattern.png',dpi=300, bbox_inches='tight')
    plt.close(fig_pattern)

    # return

    
    
    # finalizing husimi plots
    
    for ax in [ax_outer_em, ax_inner_inc]:
    
        for x in range(N_CAVITIES-1):
            ax.axvline(x+1, color='white')
        
        
        ax.set_xlim(0,N_CAVITIES)
        ax.set_ylim(-1,1)
        
        ax.set_xlabel('$s$')
        
        ax.set_yticks([-1, -0.5, 0, 0.5, 1])


    
    ax.set_ylabel('$\sin \chi_1$')

    fig_outer_em.savefig(OUTPUTDIR + 'arrow_husimi_outer_em.png')
    plt.close(fig_outer_em)
    
    
    
    ax.set_ylabel('$\sin \chi_0$')
    
    for y in np.array([-1,1]) * 1 / INDEX:
            ax_inner_inc.axhline(y, color='white', ls='--')
    
    fig_inner_inc.savefig(OUTPUTDIR + 'arrow_husimi_inner_inc.png')
    plt.close(fig_inner_inc)


##############################################
# Functions
##############################################


def rotmat(phi:float) -> np.ndarray:
    angle = -phi
    
    matrix = [
        [np.cos(angle), -np.sin(angle)],
        [np.sin(angle), np.cos(angle)]
    ]
    
    return np.array(matrix)


def find_maxima2d(array:np.ndarray, ncavities:int,q_points_per_cavity:int=100):

    p_index_list = []
    s_index_list = []
    val_list = []
    
    for i in range(ncavities):
        min_index = i*q_points_per_cavity
        max_index = (i+1)*q_points_per_cavity
        
        array_now = array[:,min_index:max_index]
        max_i, max_j = array_now.shape
    
        for p_index in range(max_i):
            for s_index in range(max_j):
                val = array_now[p_index, s_index]
                
                comparison_neighbors = []
                
                # hard boundaries for p
                if p_index > 0:
                    comparison_neighbors.append(array_now[p_index-1, s_index])
                if p_index < max_i-1:
                    comparison_neighbors.append(array_now[p_index+1, s_index])
                
                # periodic boundaries for s
                
                # left side
                if s_index == 0:
                    comparison_neighbors.append(array_now[p_index, max_j-1])
                else:    
                    comparison_neighbors.append(array_now[p_index, s_index-1])
                
                # right side
                if s_index == max_j-1:
                    comparison_neighbors.append(array_now[p_index, 0])
                else:
                    comparison_neighbors.append(array_now[p_index, s_index+1])
                
                if val > np.max(comparison_neighbors):
                    p_index_list.append(p_index)
                    s_index_list.append(s_index + i*q_points_per_cavity)
                    val_list.append(val)
                

    
    return (np.array(p_index_list), np.array(s_index_list), np.array(val_list))

if __name__ == '__main__':
    main()