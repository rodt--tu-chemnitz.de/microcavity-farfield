'''
This script reads all husimis and calculates the mean. Also plots all
husimi functions normed over the same scale, so a gif can be created.
'''

from calendar import c
from operator import index
import pandas as pd
import os
import numpy as np

import plotparams
plt = plotparams.matplotlib.pyplot

import params as pr
from calculate_husimi import HUSIMISAVEDIR, HUSIMITIMESTEPDIR, plot_husimi

SAMENORM_SAVEDIR = HUSIMISAVEDIR + 'samenorm/'


def main():
    # read used simulation params
    params = pr.load_useparams('output/params_used.json')

    # physical parameters, which are relevant for ordering data
    N_CAVITIES = params['n_cavities']
    INDEX_INNER = params['index']
 
    # searching for timesteo directories
    timestep_directory_list = sorted([f for f in os.listdir(HUSIMITIMESTEPDIR) if f.split('_')[0] == 'timestep'])
    n_timesteps = len(timestep_directory_list)
    

    # available indices for husimi function
    inout_index_list = [0,1]        # 0 -> outside, 1 -> inside
    wave_direction_list = ['inc', 'em']    # incident waves, emerging waves


    # creating directory for plots with same norm
    pr.trycreatedir(SAMENORM_SAVEDIR, print_success=True)




    for inout_index in inout_index_list:
        for wave_direction in wave_direction_list:
            husimi_name = 'husimi_{0}_{1}'.format(inout_index, wave_direction)
            husimi_csvname = husimi_name + '.csv'
            husimi_pngname = husimi_name + '.png'
            print('looking at {0}'.format(husimi_csvname))


            # we assume q and p to have the same values for all timesteps
            p_list = None
            q_list = None

            husimi_list = []

            for timestep_directory in timestep_directory_list:
                # reading csv
                fname = HUSIMITIMESTEPDIR + timestep_directory + '/' + husimi_csvname
                df = pd.read_csv(fname, sep=',', engine='python')

                # saving data
                husimi_data = df.values[:,1:]
                husimi_list.append(husimi_data)
                

                # assigning p and q for first timestep
                if p_list is None:
                    p_list = df['p'].values
                if q_list is None:
                    q_list = np.array([float(s) for s in df.keys()[1:]])



            #################################################
            # calculating necessary elements
            #################################################

            # calculating mean of husimi function
            husimi_max = np.max(husimi_list)
            husimi_mean = np.mean(husimi_list, axis=0)



            #################################################
            # plotting mean husimi function
            #################################################

            plot_husimi(
                q_list, p_list, husimi_mean, 
                inout_index=inout_index, 
                wave_direction=wave_direction, 
                savedir=HUSIMISAVEDIR,
                n_cavities=N_CAVITIES,
                index_inner=INDEX_INNER
                )

            # return



            #################################################
            # plotting functions but all with the same norm
            #################################################

            samenorm_subdir = SAMENORM_SAVEDIR + husimi_name
            pr.trycreatedir(samenorm_subdir, print_success=True)

            for timeindex, husimi in enumerate(husimi_list):
                plot_husimi(
                    q_list, p_list, husimi / husimi_max,
                    inout_index=inout_index, 
                    wave_direction=wave_direction, 
                    savedir=samenorm_subdir,
                    n_cavities=N_CAVITIES,
                    index_inner=INDEX_INNER,
                    extrasuffix='_{0:02d}'.format(timeindex),
                    vmin=0.,
                    vmax=1.
                    )
                
                
        



if __name__ == '__main__':
    main()