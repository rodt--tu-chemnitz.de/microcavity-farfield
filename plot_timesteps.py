import params as pr
import plotparams

import matplotlib
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt
import h5py
import numpy as np
import os


def main():
    print('Plotting all timesteps')
    
    ########################################
    # 1. loading parameters
    ########################################

    print('\t- load params')
    
    params = pr.load_useparams('output/params_used.json')
    
    OUTPUTDIR = params['outputfolder']

    DPML = params['dpml']
    RESOLUTION = params['resolution']

    RADIUS = params['radius']
    DIST_NFB = params['dist_nfb']
    PAD = params['pad']

    dx, dy = pr.get_cell_dimensions(params=params, withpml=False)



    ########################################
    # 2. loading and analysing data
    ########################################

    print('\t- load data')
    
    print('\t\t-> reading farfield')
    # use FF for superposition onto magnified field, so we can compare the farfield to the emerging field
    farfield_data = np.loadtxt(OUTPUTDIR + 'field_vectors.dat', dtype=np.complex128)
    angles, Ex, Ey, Ez, Hx, Hy, Hz = farfield_data
    angles = np.real(angles)
    
    Px = Ey * Hz - Ez * Hy
    Py = Ez * Hx - Ex * Hz
    Pr = np.real( np.abs( np.sqrt( np.square(Px) + np.square(Py) ) ) )
    Pr_max = np.max(Pr)
    Pr_scaled = Pr / Pr_max
    
    # print(np.max(Pr_scaled))
    
    ff_radius = np.max([dx, dy]) * .5
    
    reference_circle_x = np.cos(angles) * ff_radius
    reference_circle_y = np.sin(angles) * ff_radius
    
    ff_x = reference_circle_x * Pr_scaled
    ff_y = reference_circle_y * Pr_scaled

    # eps_filename must be determined like this because the
    # name of the file is kinda random 
    eps_filename = [
        f for f in os.listdir(OUTPUTDIR)
        if f[-3:] == '.h5' and f[:9] == 'main-eps-'
        ][0]
    
    eps_fullname = OUTPUTDIR + eps_filename
    print('\t\t-> reading ' + eps_fullname)
    eps_h5file = h5py.File(eps_fullname, 'r')
    data_eps = np.transpose(eps_h5file['eps'])

    field_fullname = OUTPUTDIR + 'main-mode_period.h5'
    print('\t\t-> reading ' + field_fullname)
    field_z_h5file = h5py.File(field_fullname, 'r')
    data_field_z = field_z_h5file[ params['component'] ]

    n_timesteps = data_field_z.shape[2]
    print('\t\t-> data contains {0} timesteps'.format(n_timesteps))


    # only search one column/row, so it doesn't take ages
    print('\t\t-> searching maximum absolute field')
    searchcolumn = data_field_z.shape[1] // 2 # look_in the middle
    ez_absmax = np.max(np.abs(data_field_z[:,searchcolumn, :]))


    searchcolumn_magnified = data_field_z.shape[1] - 1 # look at the border
    ez_absmax_magnified = np.max(np.abs(data_field_z[:,searchcolumn_magnified, :]))
    
    

    # print(data_field_z.shape)
    print('\t\tscaled with res:', np.array(data_field_z.shape)[:2] / RESOLUTION)
    print('\t\tcompare to expected dx, dy:',dx,dy)

    
    ########################################
    # 3. plotting
    ########################################

    print('\t- plotting')
    
    
    meanfield = np.zeros((data_field_z.shape[1], data_field_z.shape[0]))


    # get to display the progress percentage
    partdone_list = (np.arange(n_timesteps) + 1) / n_timesteps
    partdone_wants = np.arange(0,1.05,0.05)
    n_wants = len(partdone_wants)

    percentage_indices = []
    for want in partdone_wants: # get indices, at which the percentage is to be displayed
        templist= np.abs(partdone_list - want)
        argmin = np.argsort(templist)[0]
        percentage_indices.append(argmin)

    search_index_index = 0
    search_index_now = percentage_indices[search_index_index]
    
    for i in range(n_timesteps):
        if i == search_index_now:
            print('\t\t{0:.0f} % done'.format(partdone_list[i]*100.))
            
            search_index_index = search_index_index+1
            if search_index_index < n_wants:
                search_index_now = percentage_indices[search_index_index]

        data_now = np.transpose(data_field_z[:,:,i])
        
        meanfield = meanfield + np.square(data_now)


        # plot normal plot
        
        fig, ax = plt.subplots()

        extent=np.array([-dx,dx,-dy,dy])*.5

        ax.imshow(
            data_eps,
            cmap='binary',
            extent=extent
            )

        im = ax.imshow(
            data_now / ez_absmax,
            cmap='coolwarm',
            alpha=0.9,
            extent=extent,
            # norm=matplotlib.colors.LogNorm(
            #     vmin=1e-3,
            #     vmax=1
            #     ),
            vmin=-1,
            vmax=1
            )
        
        # plot colorbar
        if False:
            cbar = fig.colorbar(
                im,
                label=r'$E_z / | E^\mathrm{max}_z |$',
                ticks=[-1,-.5,0,.5,1],
                # orientation='horizontal'
                )


        

        # for x in [-RADIUS, RADIUS]:
        #     linedict=dict(
        #         color='black',
        #         linestyle='dashed'
        #         )
        #     ax.axvline(x, **linedict)
        #     ax.axhline(x, **linedict)

        ax.set_axis_off()
        ax.set_aspect('equal')

        fig.savefig('TEMP/img_{0:06d}.png'.format(i),dpi=200, bbox_inches='tight')
        plt.close(fig)
        
        
        # plot magnified plot
        
        fig, ax = plt.subplots()

        extent=np.array([-dx,dx,-dy,dy])*.5

        ax.imshow(
            data_eps,
            cmap='binary',
            extent=extent
            )

        im = ax.imshow(
            data_now / ez_absmax_magnified,
            cmap='coolwarm',
            alpha=0.8,
            extent=extent,
            # norm=matplotlib.colors.LogNorm(
            #     vmin=1e-3,
            #     vmax=1
            #     ),
            vmin=-1,
            vmax=1
            )
        
        ax.plot(
            ff_x,
            ff_y,
            color='black'
            )
        ax.plot(
            reference_circle_x,
            reference_circle_y,
            color='black',
            linestyle='--'
            )

        # plot colorbar
        if False:
            cbar = fig.colorbar(
                im,
                label=r'$E_z / | E^\mathrm{max}_z |$',
                ticks=[-1,-.5,0,.5,1],
                # orientation='horizontal'
                )


        

        # for x in [-RADIUS, RADIUS]:
        #     linedict=dict(
        #         color='black',
        #         linestyle='dashed'
        #         )
        #     ax.axvline(x, **linedict)
        #     ax.axhline(x, **linedict)

        ax.set_axis_off()
        ax.set_aspect('equal')

        fig.savefig('TEMP/imgmagn_{0:06d}.png'.format(i),dpi=200, bbox_inches='tight')
        plt.close(fig)


    ########################################
    # 4. calculating mean quadratic field
    ########################################
    
    print('\t- calculating mean cuadratic field')
    
    # mean over time
    meanfield = meanfield / n_timesteps
    
    np.savetxt(OUTPUTDIR + 'pattern_mean.dat', meanfield)
    
    print('\t- plotting')
    
    fig, ax = plt.subplots()
    
    ax.imshow(
        data_eps,
        cmap='binary',
        extent=extent
        )
    
    im = ax.imshow(
        meanfield / ez_absmax**2 , 
        extent=extent,
        alpha=0.8
        )

    fig.colorbar(
        im,
        label=r'$\bar{E^2} / E_\mathrm{max}^2$'
        )
    
    # ax.set_xlim(-x_pos_nfb, x_pos_nfb)
    # ax.set_ylim(-y_pos_nfb, y_pos_nfb)
    
    fig.savefig(OUTPUTDIR + 'pattern_mean.png')
    plt.close(fig)
    
    
    ########################################
    # 5. all done!
    ########################################
    
    print('DONE!')
    
    return


if __name__ == '__main__':
    main()