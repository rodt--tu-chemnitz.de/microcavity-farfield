#!/usr/bin/env bash
echo 'CREATING GIF'

echo '- deleting old pictures'
# delete old pictures
mkdir -p TEMP
rm TEMP/*.png

echo '- create new pictures'
# create neew pictures
python3 plot_timesteps.py

echo '- splicing the pictures to the gif'
convert TEMP/*.png output/dynamic.gif

echo '- done!'