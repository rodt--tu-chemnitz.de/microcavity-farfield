'''
replaces json file
'''

from limacon_class import Limacon
import numpy as np
import json
import os

##########################################
# 1. Parmameter Dict
##########################################

params_default = {
    # cell parameters
    "dpml" :                2.0,
    "dist_nfb" :            3.0,
    "pad" :                 0.05,
    "resolution" :          100,
    "calctime":             1e2,
    "symmetry":             "symmetric",
    
    # source parameters
    # "fcen" :                0.9545088375858142,
    "fcen" :                0.999,
    "df" :                  1.0,
    "source_angle":         0.0,
    "source_r":             0.95,
    "component" :           "ez",
    "sourcetype" :          "point",
    
    # cavity parameters
    "n_cavities" :          1,
    "intercavity_dist" :    0.1,
    "radius" :              1.0,
    "deform" :              0.0,
    "index":                3.0,
    
    # output parameters
    "outputfolder":     "output/",
    }


##########################################
# 2. Determine cell dimensions, is the same for all scripts
##########################################

def test_limacon(params:dict=params_default) -> Limacon:
    '''
    gives limacon from which certain parameters can be taken
    '''

    l = Limacon(
        radius=params['radius'],
        deform=params['deform'],
        material=None,
        height=1.
        )
    return l



def get_limacon_centerdist(params:dict=params_default) -> float:
    '''
    gives distance between limacons
    '''
    
    # use test limacon to obtain information about cell size
    l = test_limacon(params=params)
    limacon_dy = l.maxlateral
    limacon_centerdist = params['intercavity_dist'] + limacon_dy * 2.
    
    return limacon_centerdist



def get_cell_dimensions(params:dict=params_default, withpml:bool=True) -> np.ndarray:
    '''
    Returns cell size with PML
    '''
    
    # geometry params
    DPML = params['dpml']
    RADIUS = params['radius']
    N_CAVITIES = params['n_cavities']
    CAVITY_DIST = params['intercavity_dist']
    PAD = params['pad']
    DIST_NFB = params['dist_nfb']
    

    limacon_centerdist = get_limacon_centerdist(params=params)
    limacon_dy = (limacon_centerdist - CAVITY_DIST) * .5
    
    
    # determine cell size

    dx = 2. * ( RADIUS + DIST_NFB + PAD)
    dy = limacon_centerdist * (N_CAVITIES - 1) + 2. * (limacon_dy + DIST_NFB + PAD)
    
    if withpml:
        dx += 2 * DPML
        dy += 2 * DPML
    
    return np.array( (dx, dy) )



##########################################
# 3. useparams file
##########################################

def save_useparams(params:dict, fname:str='params_used.json') -> None:
    '''
    saves dictionary of used params to file
    '''
    with open(fname, 'w', encoding='utf-8') as f:
        json.dump(params, f, ensure_ascii=False, indent=4)


def load_useparams(fname:str='params_used.json') -> dict:
    '''
    reads used params and returns them as a dict
    '''
    with open(fname, 'r') as f:
        params = json.load(f)
    return params

##########################################
# 4. some flags for all output
##########################################

# determines of plotting is enabled
PLOTTING = True

# determines if all field components are stored on top of pattern at the end
ALLCOMPONENTS = False




##########################################
# 5. useful functions
##########################################

def trycreatedir(path:str, print_success:bool=False) -> int:
    '''
    tries to create directory and displays message

    returned string corresponds to an (un)succesful operation
    '''
    try:
        os.makedirs(path)
    except OSError:
        if print_success: print('{0} already exists'.format(path))
        return 1
    else:
        if print_success: print('{0} created'.format(path))
        return 0