import matplotlib

# latex text options
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.family'] = 'serif'

# ticks
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['xtick.top'] = True
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['ytick.right'] = True


# figure
matplotlib.rcParams['figure.figsize'] = (5., 3.)
matplotlib.rcParams['savefig.dpi'] = 300.
matplotlib.rcParams['savefig.bbox'] = 'tight'
matplotlib.rcParams['savefig.pad_inches'] = 0.1

# colormap
from matplotlib.pyplot import get_cmap
cmap = get_cmap('viridis')