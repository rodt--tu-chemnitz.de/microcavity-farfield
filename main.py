'''
This is the main file for the repo, where meep is utilized.
'''
import params as pr

from limacon_class import Limacon

import meep as mp
import numpy as np
import sys
import getopt

def main():
    
    #########################################
    # 1. set general parameters 
    #########################################

    #----------------------------
    # 1.1 read parameters
    #----------------------------

    params = pr.params_default

    RESOLUTION = params['resolution']
    DPML = params['dpml']


    # source parameters
    SOURCE_ANGLE = params['source_angle']
    SOURCE_R = params['source_r']
    
    FCEN = params['fcen']
    DF = params['df']
    
    component = params['component']
    if component == 'ez':
        source_comp = mp.Ez
        output_func = mp.output_efield_z
    elif component == 'hz':
        source_comp = mp.Hz
        output_func = mp.output_hfield_z
    else:
        print('ERROR: component {0} not recognized'.format(component))
        raise ValueError
    
    SOURCETYPE = params['sourcetype']

    CALCTIME = params['calctime']
    
    SYMMETRY = params['symmetry']


    # geometry params
    RADIUS = params['radius']
    DEFORM = params['deform']
    N_CAVITIES = params['n_cavities']
    CAVITY_DIST = params['intercavity_dist']
    
    PAD = params['pad']
    DIST_NFB = params['dist_nfb']
    INDEX = params['index']
    
    # where to save
    OUTPUTDIR = params['outputfolder']

    #----------------------------
    # 1.2 parse command line options
    #----------------------------
    
    # the advantage in this is, that this way the command line arguments don't need
    # to be ordered and the input has much higher readability
    
    # read options
    options, remainder = getopt.getopt(
        sys.argv[1:],     # available arguments
        '',               # option definition string for single characters
        [                 # sequence of long-style option names
            'deform=',              # deform value of limacons
            'ncavities=',           # number of limacons
            'cavitydist=',          # distance between limacons
            'fcen=',                # frequency
            'df=',                  # frequency width of source
            'calctime=',            # calculation time after sources
            'resolution=',          # resolution of cell
            'saveallcomponents=',   # save all field components for further study
            'sourcetype=',          # distinguish between normal point source and planewave
            'symmetry=',            # symmetry of cell along y axis, is either "symmetric" or "antisymmetric"
            'outputdir='            # directory, where all results are to be saved
        ]
        )
    
    
    # parse options
    for opt, arg in options:
        if opt in ('--deform'): # write options as lists, in case i want to add single-letter arguments later
            DEFORM = float(arg)
            params['deform'] = DEFORM
        elif opt in ('--ncavities'):
            N_CAVITIES = int(arg)
            params['n_cavities'] = N_CAVITIES
        elif opt in ('--cavitydist'):
            CAVITY_DIST = float(arg)
            params['intercavity_dist'] = CAVITY_DIST
        elif opt in ('--fcen'):
            FCEN = float(arg)
            params['fcen'] = FCEN
        elif opt in ('--df'):
            DF = float(arg)
            params['df'] = DF
        elif opt in ('--calctime'):
            CALCTIME = float(arg)
            params['calctime'] = CALCTIME
        elif opt in ('--resolution'):
            RESOLUTION = float(arg)
            params['resolution'] = RESOLUTION
        elif opt in ('--saveallcomponents'):
            pr.ALLCOMPONENTS = bool(arg)
        elif opt in ('--sourcetype'):
            SOURCETYPE = str(arg)
            params['sourcetype'] = SOURCETYPE
        elif opt in ('--symmetry'):
            if arg in ['symmetric', 'antisymmetric']:
                SYMMETRY = arg
                params['symmetry'] = SYMMETRY
            else:
                print('ERROR: symmetry {0} unknown!'.format(arg))
                raise ValueError
        elif opt in ('--outputdir'):
            OUTPUTDIR = arg
            params['outputfolder'] = OUTPUTDIR
            
            
        
    if N_CAVITIES == 1 and SYMMETRY == 'antisymmetric':
        '''
        Harmonic inversion doesn't find resonant frequencies as the field is grabbed directly in the
        plane of symmetry, where the field is always zero. I could fix this, but there are more pressing
        matters right now.
        '''
        print('ERROR: combination of parameters invalid, harminv does not work correctly like this')
        print('To fix this, harminv needs to be applied at another location than the plane of symmetry.')
        raise NotImplementedError
    
    
    # saving used params
    pr.save_useparams(params, fname='output/params_used.json')


    #----------------------------
    # 1.3 print parameters
    #----------------------------
    
    print('---- PARAMETERS ----')
    
    for key, val in params.items():
        print('{0} : {1}'.format(key, val))

    #----------------------------
    # 1.4 define cell
    #----------------------------
    
    # use test limacon to obtain information about cell size
    test_limacon = Limacon(
        radius=RADIUS,
        deform=DEFORM,
        material=mp.Medium(index=INDEX),
        height=1.
        )
    limacon_dy = test_limacon.maxlateral
    limacon_centerdist = CAVITY_DIST + limacon_dy * 2.
    
    
    # determine cell size
    cell_size_nodpml = mp.Vector3(
        *pr.get_cell_dimensions(params=params, withpml=False)
        )
    cell_size = mp.Vector3(
        *pr.get_cell_dimensions(params=params, withpml=True)
        )

    #########################################
    # 2. create geometry
    #########################################

    # the following code can only be used if symmetries apply
    # which are turned off intentionally now. The idea is, that like this
    # you have an extra numerical check in place.

    n_cavities_simulated = N_CAVITIES // 2 + N_CAVITIES % 2
    print('\n=> {0} cavities need to be simulated'.format(n_cavities_simulated))
    
    
    if N_CAVITIES % 2 == 0:
        center_ylist = np.array([limacon_centerdist * (i + 0.5) for i in np.arange(n_cavities_simulated)])
    else:
        center_ylist = np.array([limacon_centerdist * i for i in np.arange(n_cavities_simulated)])
    

    # center_ylist = ( np.arange(N_CAVITIES) - (N_CAVITIES-1) * .5 ) * limacon_centerdist
    print('\tcenter y-coordinates: {0}'.format(center_ylist))

    geometry = []
    for y in center_ylist:
        geom_center = mp.Vector3(x=0, y=y)
        print('\t-> adding limacon at {0}'.format(geom_center))
        disc = Limacon(
            radius=RADIUS,
            deform=DEFORM,
            material=mp.Medium(index=INDEX),
            center=geom_center
            ).Prism
        geometry.append(disc)
    
    # geometry = []


    #########################################
    # 3. create source
    #########################################

    # the following needs to be defined always, as source_center
    # is accessed later for harminv function
    
    source_radius = SOURCE_R * RADIUS
    source_angle = SOURCE_ANGLE * np.pi

    # source pos for one resonator
    source_xpos_0 = source_radius * np.cos(source_angle)
    source_ypos_0 = source_radius * np.sin(source_angle)
    source_center = mp.Vector3(source_xpos_0, source_ypos_0)


    sources = []
    # in each microcavity a source is placed
    if SOURCETYPE == 'point':
        for y in center_ylist:
            src = mp.Source(
                mp.GaussianSource(
                    FCEN,
                    fwidth=DF,
                    ),
                component=source_comp,
                center=source_center + mp.Vector3(x=0, y=y),
                )
            sources.append(src)
            
    # a planewave with specified frequency comes from the left
    # not that it still has a gaussian wave profile
    elif SOURCETYPE == 'planewave':
        planewave_center = mp.Vector3(x=-cell_size_nodpml.x*.5, y=0)
        planewave_size = mp.Vector3(x=0, y=cell_size_nodpml.y)
        
        src = mp.Source(
            mp.GaussianSource(
                FCEN,
                fwidth=DF,
                ),
            component=source_comp,
            center=planewave_center,
            size=planewave_size
            )
        sources.append(src)
        
    else:
        print('ERROR: source type "{0}" not known'.format(SOURCETYPE))
        raise ValueError

    #########################################
    # 4. simulation
    #########################################

    print('\nRUN 1: running sources...\n')

    # by default: even modes in y-direction
    mirrors = []
    if SYMMETRY == 'symmetric':
        mirrors.append(mp.Mirror(mp.Y, phase=+1))
    elif SYMMETRY == 'antisymmetric':
        mirrors.append(mp.Mirror(mp.Y, phase=-1))
    else:
        print('ERROR: symmetry {0} unknown'.format(SYMMETRY))
        print('This should have been caught in arg parsing!!!')
        raise ValueError

    # if perfect discs are considered, only even modes in x-direction
    # if DEFORM < 1e-3:
    #     mirrors.append(mp.Mirror(mp.X, phase=1))
    
    # def simulation
    sim = mp.Simulation(
        cell_size=cell_size,
        resolution=RESOLUTION,
        geometry=geometry,
        sources=sources,
        boundary_layers=[mp.PML(DPML)],
        symmetries=mirrors,
        force_complex_fields=False
        )

    # run simulation
    sim.run(
        mp.in_volume(
            mp.Volume(
                mp.Vector3(), 
                size=cell_size_nodpml
                ),
            mp.at_beginning(mp.output_epsilon),
            ),
        # mp.at_beginning(mp.output_epsilon),
        # mp.to_appended("field_z", mp.at_every(1/FCEN, output_func)),
        until_after_sources=1/FCEN
        )
    
    
    #########################################
    # 4.1 adding dft fields for following stuff
    #########################################
    
    # doesn't work for now, no idea what the appropriate syntax is for this
    
    # dftfields = sim.add_dft_fields(
    #     [component], 
    #     FCEN, 
    #     DF,
    #     1,
    #     # center=mp.Vector3(),
    #     # size=cell_size_nodpml,
    #     yee_grid=True
    #     )
    
    # sim.run(until=100/FCEN)
    
    # # dft fields
    # print('saving dft fields')
    # only outputs a single number, no idea why
    # dftfield_array = sim.get_dft_array(dftfields, mp.Ez, 1)
    # print(dftfield_array)
    # np.savetxt('dftfields.dat', dftfield_array)
    
    # doesn't yield error but also doesn't appear to do anything
    # sim.output_dft(dftfields, 'dftfields_2')
    
    #########################################
    # 5. saving output (until after sources)
    #########################################

    dielectric = sim.get_array(
        component=mp.Dielectric,
        size=cell_size_nodpml,
        center=mp.Vector3()
        )
    dielectric = np.transpose(dielectric)

    field_z = sim.get_array(
        component=source_comp,
        size=cell_size_nodpml,
        center=mp.Vector3()
        )
    field_z = np.transpose(field_z)

    np.savetxt(OUTPUTDIR + 'geometry.dat', dielectric)
    np.savetxt(OUTPUTDIR + 'pattern_start.dat', field_z)

    #########################################
    # 6. define nearfield box
    #########################################

    print('\n-> running farfields...\n')


    nfb_dx = cell_size_nodpml.x - PAD # cell in nearfield-box
    nfb_dy = cell_size_nodpml.y - PAD

    nearfield_box = sim.add_near2far(
        FCEN, 0, 1,
        mp.Near2FarRegion( # up
            center=mp.Vector3(0,+0.5*nfb_dy), size=mp.Vector3(nfb_dx,0), weight=+1
            ),
        mp.Near2FarRegion( # down
            center=mp.Vector3(0,-0.5*nfb_dy), size=mp.Vector3(nfb_dx,0), weight=-1
            ),
        mp.Near2FarRegion( # right
            center=mp.Vector3(+0.5*nfb_dx,0), size=mp.Vector3(0,nfb_dy), weight=+1
            ),
        mp.Near2FarRegion( # left
            center=mp.Vector3(-0.5*nfb_dx,0), size=mp.Vector3(0,nfb_dy), weight=-1
            )
        )

    flux_box = sim.add_flux(FCEN, 0, 1,
        mp.FluxRegion( # up
            center=mp.Vector3(0,+0.5*nfb_dy), size=mp.Vector3(nfb_dx,0), weight=+1
            ),
        mp.FluxRegion( # down
            center=mp.Vector3(0,-0.5*nfb_dy), size=mp.Vector3(nfb_dx,0), weight=-1
            ),
        mp.FluxRegion( # right
            center=mp.Vector3(+0.5*nfb_dx,0), size=mp.Vector3(0,nfb_dy), weight=+1
            ),
        mp.FluxRegion( # left
            center=mp.Vector3(-0.5*nfb_dx,0), size=mp.Vector3(0,nfb_dy), weight=-1
            )
        )

    #########################################
    # 7. run
    #########################################
    
    # 7.1 record mode pattern

    print('\nRUN 2: recording mode pattern')
    
    modeperiod_datapoints = 40
    delta_t_mode = 1./(FCEN*modeperiod_datapoints)

    sim.run(
        mp.synchronized_magnetic(
            mp.in_volume(
                mp.Volume(
                    mp.Vector3(), 
                    size=cell_size_nodpml
                    ),
                mp.to_appended(
                    'mode_period', 
                    mp.at_every(
                        delta_t_mode, 
                        mp.output_efield_z,
                        mp.output_poynting
                        )
                    ),
                ),
            ),
        
        # maxtime
        until=1./FCEN,
        )
    
    
    # 7.2 run farfields
    
    # use upper limacon
    harminv_loc = source_center + mp.Vector3(x=0, y=center_ylist[-1])
    
    print('\nRUN 3: grabbing frequencies at ({0:.2f}, {1:.2f})'.format(harminv_loc.x, harminv_loc.y))

    sim.run(
        mp.Harminv(source_comp, harminv_loc, FCEN, DF),
        # positional arguments
        # mp.at_beginning(mp.output_epsilon),
        # mp.to_appended("field_z", mp.at_every(CALCTIME/FCEN/100, output_func)),
        
        mp.in_volume(
            mp.Volume(
                mp.Vector3(x=harminv_loc.x), 
                size=mp.Vector3(y=cell_size.y)
                ),
            mp.to_appended(
                'field_at_harminv', 
                mp.at_every(
                    1./(FCEN*10.), # 10 datapoints per period
                    mp.output_efield_z)
                )
            ),
        
        # output dft
        # mp.at_end(mp.output_dft(dftfields, 'dftfields_1')),
        
        # keyword arguments
        until=CALCTIME/FCEN,
        # until_after_sources=mp.stop_when_fields_decayed(100, mp.Ez, source_center, 1e-3)
        )


    #########################################
    # 8. calculate far field
    #########################################

    near_flux = mp.get_fluxes(flux_box)[0]

    r = 1000/FCEN      # half side length of far-field square box OR radius of far-field circle
    res_ff = 1         # resolution of far fields (points/μm)
    far_flux_box = (
        + nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=r), size=mp.Vector3(2*r)), res_ff)[0]
        - nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=-r), size=mp.Vector3(2*r)), res_ff)[0]
        + nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(r), size=mp.Vector3(y=2*r)), res_ff)[0]
        - nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(-r), size=mp.Vector3(y=2*r)), res_ff)[0]
        )

    npts = 1000         # number of points in [0,2*pi) range of angles
    angles = 2*np.pi/npts*np.arange(npts)

    field_vectors = []
    for angle in angles:
        ff = sim.get_farfield(
            nearfield_box,
            mp.Vector3(r*np.cos(angle), r*np.sin(angle)))
        field_vectors.append(ff)

    field_vectors = np.transpose(field_vectors)
    np.savetxt(OUTPUTDIR + 'field_vectors.dat', [angles, *field_vectors])

    Ex, Ey, Ez, Hx, Hy, Hz = field_vectors

    # P = E* x H

    Ex, Ey, Ez = np.conj([Ex, Ey, Ez])

    Px = np.real(Ey*Hz-Ez*Hy)
    Py = np.real(Ez*Hx-Ex*Hz)
    Pr = np.sqrt(np.square(Px)+np.square(Py))

    far_flux_circle = np.sum(Pr)*2*np.pi*r/len(Pr)

    print("flux:, {:.6f}, {:.6f}, {:.6f}".format(near_flux,far_flux_box,far_flux_circle))


    #########################################
    # 9. saving output (after farfield calculation)
    #########################################
    
    if pr.ALLCOMPONENTS:
        print('saving all field components...')
        field_components = [
            (mp.Ex, 'Ex'), 
            (mp.Ey, 'Ey'), 
            (mp.Ez, 'Ez'), 
            (mp.Hx, 'Hx'), 
            (mp.Hy, 'Hy'), 
            (mp.Hz, 'Hz')
            ]
        
        for component, name in field_components:
            field = sim.get_array(
                component=component,
                size=cell_size_nodpml,
                center=mp.Vector3()
                )
            field = np.transpose(field)
            np.savetxt(OUTPUTDIR + 'pattern_{0}.dat'.format(name), field)
    
    print('saving source component...')
    field_z = sim.get_array(
        component=source_comp,
        size=cell_size_nodpml,
        center=mp.Vector3()
        )
    field_z = np.transpose(field_z)
    np.savetxt(OUTPUTDIR + 'pattern_end.dat', field_z)





if __name__ == '__main__':
    main()