'''
Performs the complete calculation of the husimi function for
specific values of t & s
'''


import numpy as np
import os
import pandas as pd

import params as pr
import plotparams
plt = plotparams.matplotlib.pyplot
Line2D = plotparams.matplotlib.lines.Line2D


PLOTTING = True             # create plots of how husimi functions come to be at each step
LOGGING_STEPS = False        # log individual steps

DATADIR = 'output/'
BORDERFUNCDIR = DATADIR + 'borderfunctions_period/'
HUSIMISAVEDIR = DATADIR + 'husimi/'
HUSIMITIMESTEPDIR = HUSIMISAVEDIR + 'timesteps/'
HUSIMIPLOTTINGDIR = HUSIMISAVEDIR + 'grafics/'

####################################################################
# Husimi
####################################################################

def main():


    print('reading params...')

    params = pr.load_useparams(DATADIR + 'params_used.json')
    N_CAVITIES = params['n_cavities']
    FCEN = params['fcen']

    # refractive indices
    INDEX_INNER = params['index']
    INDEX_OUTER = 1.

    # maximum impulses inside and outside
    P_MAX_INNER = FCEN * 2.*np.pi * INDEX_INNER
    P_MAX_OUTER = FCEN * 2.*np.pi * INDEX_OUTER

    # factors for finished husimi functions
    HUSIMIFACTOR_OUTER = P_MAX_OUTER / (2.*np.pi)
    HUSIMIFACTOR_INNER = P_MAX_INNER / (2.*np.pi)



    # obtain timesteps
    print('searching files...')
    filenames_borderfunctions = np.array(sorted([f for f in os.listdir(BORDERFUNCDIR) if f[-4:] == '.dat']))
    n_timesteps = filenames_borderfunctions.size
    
    print('\t{0} timesteps found\n'.format(n_timesteps))

    # iterating
    for timeindex, fname in enumerate(filenames_borderfunctions):
        print('timestep {0}/{1}'.format(timeindex+1, n_timesteps))

        
        # everything gets saved per timestep

        # directory to save husimi functions
        timestepdir_now = HUSIMITIMESTEPDIR + '/timestep_{0:02d}/'.format(timeindex)
        pr.trycreatedir(timestepdir_now)

        # directories to save optional plots
        timestep_plottingdir = HUSIMIPLOTTINGDIR + 'timestep_{0:02d}/'.format(timeindex)

        plottingdir_inner = timestep_plottingdir + 'inner/'
        plottingdir_outer = timestep_plottingdir + 'outer/'
        if PLOTTING:
            pr.trycreatedir(plottingdir_inner)
            pr.trycreatedir(plottingdir_outer)



        ####################################################################
        # load data
        ####################################################################
        
        fname_complete = BORDERFUNCDIR + fname
        if LOGGING_STEPS: print('-> loading borderfunctions from {0}'.format(fname_complete))


        data = np.loadtxt(fname_complete)
        s_listlist,          field_normal_listlist =     cut_data(s_list=data[0], y_list=data[1], ncavities=N_CAVITIES)
        _,                  field_der_inner_listlist =  cut_data(s_list=data[0], y_list=data[2], ncavities=N_CAVITIES)
        _,                  field_der_outer_listlist =  cut_data(s_list=data[0], y_list=data[3], ncavities=N_CAVITIES)
        data_listlist = [field_normal_listlist, field_der_inner_listlist, field_der_outer_listlist]


        
        ####################################################################
        # fourier spectra
        ####################################################################
        
        if PLOTTING:
            if LOGGING_STEPS: print('\t-> plotting fourier spectra')
            
            
            fig, axes = plt.subplots(
                figsize=(6,4),
                nrows=3,
                sharex=True
                )
            
            
            # compare
            for ax, y_listlist in zip(axes, data_listlist):
                
                # determine spectra
                for ii, (s_list, y_list) in enumerate(zip(s_listlist, y_listlist)):
                    # determine sampling
                    s_delta = np.mean(s_list[1:] - s_list[:-1])
                    freqs = np.fft.fftshift(np.fft.fftfreq(s_list.shape[0], d=s_delta))

                    fourier_spectrum = np.fft.fft(y_list)
                    fourier_spectrum = np.fft.fftshift(fourier_spectrum)
                    fourier_spectrum_abs = np.abs(fourier_spectrum)
                    
                    max_spectrum = np.max(fourier_spectrum_abs)
                    yshift = (ii+1) * max_spectrum * .25 # vertical shift, so lines can be distinguished
                    yshift = 0
                    
                    ax.plot(
                        freqs, 
                        fourier_spectrum_abs + yshift,
                        color='C{0}'.format(ii), 
                        label='array {0}'.format(ii+1)
                        )

                    
            axes[1].legend(loc='center', framealpha=1.)
            
            # cosmetics
            ylabel_list = ['field', 'inner der', 'outer der']
            for ax, ylabel in zip(axes, ylabel_list):
                ax.set_xlim(-25, 25)

                ax.set_ylabel(ylabel)
            
            plt.tight_layout()
            fig.savefig(timestep_plottingdir + 'husimiprod_1_fourier_spectrum.png')
            plt.close(fig)
        
        
        
        ####################################################################
        # application of coherent state
        ####################################################################
        
        coherent_state_broaden = 1      # more datapoints for coherent state so inner husimi is smoother


        n_qpoints = 100
        q_uselist = np.arange(n_qpoints) / n_qpoints
        # q_uselist = np.array([0.00, 0.50])

        

        for inout_index, impulse_norm, index_use, husimi_factor, plottingdir in zip(
                [0,1],
                [P_MAX_OUTER, P_MAX_INNER],
                [INDEX_OUTER, INDEX_INNER],
                [HUSIMIFACTOR_OUTER, HUSIMIFACTOR_INNER],
                [plottingdir_outer, plottingdir_inner]
                ):
            
            if LOGGING_STEPS:
                print('-> parameters for now:')
                # print('\tq_use\t\t: {0}'.format(q_use))
                print('\tinout\t\t: {0}'.format(inout_index))
                print('\timpulse_norm\t: {0:.6f}'.format(impulse_norm))
                print('\trefract. index\t: {0}'.format(index_use))


            husimi_inc_list = []
            husimi_em_list = []


            for q_index, q_use in enumerate(q_uselist):
                if LOGGING_STEPS: print('\tq_use\t\t: {0}'.format(q_use))
                img_suffix = '_j={0}_q={1:.3f}.png'.format(inout_index, q_use)
                
                inout_index_string = r"{0}".format(inout_index)
                
                #---------------------------------------
                # wave function
                #---------------------------------------
                
                # CALCULATION
                if LOGGING_STEPS: print('\t\t-> performing coherent state calculations')
                
                
                
                # compare
                coh_data = []
                spectrum_normal_data = []
                
                for cavity_index in range(N_CAVITIES):
                    
                    # coherent state
                    
                    x_list = s_listlist[cavity_index]
                    y_list = field_normal_listlist[cavity_index]
                    s_coh, y_coh = apply_coherent_state(q=q_use, s=x_list-cavity_index, y=y_list, broaden=coherent_state_broaden)

                    coh_data.append([s_coh, y_coh])
                    
                    
                    # fft
                    freqs_normal, spectrum_normal = complete_fft(s_coh, y_coh)
                    impulses_normal = freqs_normal / impulse_norm
                    
                    
                    # consider only possible impulses
                    impulses_normal_bool = np.abs(impulses_normal) < 1. 
                    impulses_normal_use = impulses_normal[impulses_normal_bool]
                    spectrum_normal_use = spectrum_normal[impulses_normal_bool]

                    # saving
                    spectrum_normal_data.append(
                        [impulses_normal_use, spectrum_normal_use]
                        )
                
                coh_data = np.array(coh_data)
                spectrum_normal_data = np.array(spectrum_normal_data)
                spectrum_normal_list = spectrum_normal_data[:,1] # only spectra, as impulses are identical for all data

                    
                    
                    
                # PLOTS
                
                if PLOTTING:
                    if LOGGING_STEPS: print('\t\t-> plotting wave function and fft')
                    
                    fig, axes = plt.subplots(
                        nrows=2,
                        ncols=N_CAVITIES,
                        sharey='row',
                        figsize=(N_CAVITIES*3, 4)
                        )
                    
                    # coherent state : first row
                    # fft of coherent state : 2nd row
                    # 
                    # distinction between cases necessary, as for one cavity the axes have shape [ax, ax, ...],
                    # while for more cavities it has [[ax, ax, ...], [ax, ax, ...]] 
                    
                    if N_CAVITIES == 1:
                        ax_coh_list = [axes[0]]
                        ax_cohfft_list = [axes[1]]
                    else:
                        ax_coh_list = axes[0]
                        ax_cohfft_list = axes[1]

                    # coherent state

                    for ax, (s_coh, y_coh) in zip(ax_coh_list, coh_data):
                        ax.plot(
                            s_coh, y_coh,
                            )
                    

                    
                    # FFT
                    
                    for ax, (impulses, spectrum) in zip(ax_cohfft_list, spectrum_normal_data):
    
                        # compare
                        ax.semilogy(
                            np.real(impulses), # has data type complex, as whole spectrum_normal_data is complex due to spectrum, thus imag(p) = 0
                            np.abs(spectrum),
                            )

                    
                    # labels and stuff

                    ax_coh_list[0].set_ylabel('wave function')
                    ax_cohfft_list[0].set_ylabel('abs fft comp')

                    for cavity_index, ax in enumerate(ax_coh_list):
                        ax.set_xlabel('$s$')
                        ax.set_title('cavity \# {0}'.format(cavity_index))

                    for ax in ax_cohfft_list:
                        ax.set_xlabel('$p$')
                        ax.set_xlim(-1,1)
                    
                    # saving

                    plt.tight_layout()
                    
                    fig.savefig(plottingdir + 'husimiprod_2_coherent_state_wavefunction' + img_suffix)
                    plt.close(fig)

                
                #---------------------------------------
                # derivative
                #---------------------------------------
                
                
                # CALCULATION
                if LOGGING_STEPS: print('\t\t-> performing derivative calculations')
                

                
                # compare
                
                coh_der_data = []
                spectrum_der_data = []
                for cavity_index in range(N_CAVITIES):
                    
                    # coherent state
                    x_list = s_listlist[cavity_index]
                    y_list = field_der_inner_listlist[cavity_index]
                    s_coh_der, y_coh_der = apply_coherent_state(q=q_use, s=x_list-cavity_index, y=y_list, broaden=coherent_state_broaden)
                    
                    coh_der_data.append([s_coh_der, y_coh_der])

                    # fft
                    freqs_der, spectrum_der = complete_fft(s_coh_der, y_coh_der)
                    impulses_der = freqs_der / impulse_norm
                    
                    # consider only possible impulses
                    impulses_der_bool = np.abs(impulses_der) < 1. 
                    impulses_der_use = impulses_der[impulses_der_bool]
                    spectrum_der_use = spectrum_der[impulses_der_bool]

                    # saving
                    spectrum_der_data.append(
                        [impulses_der_use, spectrum_der_use]
                        )
                    
                coh_der_data = np.array(coh_der_data)
                spectrum_der_data = np.array(spectrum_der_data)
                spectrum_der_list = spectrum_der_data[:,1]


                
                # PLOTTING
                
                
                if PLOTTING:
                    if LOGGING_STEPS: print('\t\t-> plotting derivative and fft')
                    
                    fig, axes = plt.subplots(
                        nrows=2,
                        ncols=N_CAVITIES,
                        sharey='row',
                        figsize=(N_CAVITIES*3, 4)
                        )
                    

                    
                    # coherent state : first row
                    # fft of coherent state : 2nd row
                    # 
                    # distinction between cases necessary, as for one cavity the axes have shape [ax, ax, ...],
                    # while for more cavities it has [[ax, ax, ...], [ax, ax, ...]] 
                    
                    if N_CAVITIES == 1:
                        ax_coh_list = [axes[0]]
                        ax_cohfft_list = [axes[1]]
                    else:
                        ax_coh_list = axes[0]
                        ax_cohfft_list = axes[1]

                    # coherent state
                    
                    for ax, (s, y) in zip(ax_coh_list, coh_der_data):

                        ax.plot(
                            s, y,
                            )
                    
                    
                    # FFT
                    
                    for ax, (impulses, spectrum) in zip(ax_cohfft_list, spectrum_der_data):
                        # compare
                        ax.semilogy(
                            np.real(impulses), # imag(p) = 0, due to the list only being complex because of listlist
                            np.abs(spectrum),
                            )
                    
                    
                    # labels and stuff
                    
                    ax_coh_list[0].set_ylabel('wave function der')
                    ax_cohfft_list[0].set_ylabel('abs fft comp')

                    for cavity_index, ax in enumerate(ax_coh_list):
                        ax.set_xlabel('$s$')
                        ax.set_title('cavity \# {0}'.format(cavity_index))

                    for ax in ax_cohfft_list:
                        ax.set_xlabel('$p$')
                        ax.set_xlim(-1,1)
                    
                    # saving

                    plt.tight_layout()
                    
                    fig.savefig(plottingdir + 'husimiprod_3_coherent_state_derivative' + img_suffix)
                    plt.close(fig)

                    
                
                ####################################################################
                # angular momentum weights
                ####################################################################
                
                if LOGGING_STEPS: print('\t\t-> plotting angular momentum weights')

                '''
                F_j = sqrt( n_j cos(chi_j) )
                '''
                # only needs to be plotted the first time
                
                if q_index == 0:
                    # CALCULATION
                    
                    # normal
                    weights_normal  = np.sqrt( index_use * np.cos(np.arcsin(impulses_normal_use)) )
                    
                    # derivatives
                    weights_der  = 1.j / ( P_MAX_OUTER * weights_normal )
                    
                    if PLOTTING:
                    
                        fig, axes = plt.subplots(
                            figsize=(4,4),
                            nrows=2, 
                            sharex='all'
                            )
                        
                        for ax, weights, label in zip(
                                axes,
                                [weights_normal, weights_der],
                                [
                                    r"$h$: $W = F_" + inout_index_string + r"= \sqrt{n_" + inout_index_string + r"\cos \chi_" + inout_index_string + r"}$",
                                    r"$h'$: $W = i / k_0 F_" + r"{0}$".format(inout_index)
                                ]
                                ):
                            
                            ax.plot(impulses_normal_use, np.abs(weights))
                        
                        
                            ax.set_ylabel(label)
                            ax.set_xlim(-1,1)
                        
                        
                        axes[-1].set_xlabel(r'$\sin \chi$')
                        
                        plt.tight_layout()
                        fig.savefig(plottingdir + 'husimiprod_4_angular_momentum_weights.png')
                        plt.close(fig)

                    # weights and impulses are the same for all data
                    # suffix henceforth, as it is from here on used for all datasets
                    # and to avoid confusion with other impulse variables above
                    impulses_henceforth = impulses_normal_use
                
                
                
                ####################################################################
                # angular momentum weights and
                ####################################################################

                # CALCULATION
                if LOGGING_STEPS: print('\t\t-> calculating husimi parts')
                
                
                # compare
                hpart_normal_list = []
                hpart_der_list = []
                for cavity_index, (spectrum_normal, spectrum_der) in enumerate(zip(spectrum_normal_data[:,1], spectrum_der_data[:,1])):
                    # normal
                    hpart_normal = spectrum_normal * weights_normal
                    hpart_normal_list.append(hpart_normal)
                    
                    # derivative
                    hpart_der = spectrum_der * weights_der
                    hpart_der_list.append(hpart_der)
                    
                hpart_normal_list = np.array(hpart_normal_list)
                hpart_der_list = np.array(hpart_der_list)
                    
                    
                
                # PLOTTING
                if PLOTTING:
                    if LOGGING_STEPS: print('\t\t-> plotting constituents of husimi parts')

                    fig, axes = plt.subplots(
                        nrows=2, 
                        ncols=N_CAVITIES, 
                        figsize=((N_CAVITIES)*3,4),
                        sharey='row',
                        sharex='all'
                        )

                    # iteration-order: 0. normal, 1. derivatives

                    style_spectrum = dict(color='C0', linestyle='--')
                    style_weights = dict(color='red', linestyle=':')
                    style_hpart = dict(color='black', linestyle='-')


                    # cases, so the dimensions of axes stay the same
                    if N_CAVITIES == 1:
                        axes_normal = [axes[0]]
                        axes_der = [axes[1]]
                    else:
                        axes_normal = axes[0]
                        axes_der = axes[1]

                    # for array
                    for cavity_index, (ax_normal, ax_der, hpart_normal, spectrum_normal, hpart_der, spectrum_der) in enumerate(zip(
                            axes_normal,
                            axes_der,
                            hpart_normal_list, 
                            spectrum_normal_list,
                            hpart_der_list, 
                            spectrum_der_list
                            )):



                        # plot normal
                        ax_normal.semilogy(impulses_henceforth, np.abs(hpart_normal), **style_hpart)
                        ax_normal.semilogy(impulses_henceforth, np.abs(spectrum_normal), **style_spectrum)

                        # weights
                        ax_normal_weights = ax_normal.twinx()
                        ax_normal_weights.plot(impulses_henceforth, np.abs(weights_normal), **style_weights)
                        # style
                        ax_normal_weights.tick_params(
                            color=style_weights['color'],
                            labelcolor=style_weights['color']
                            )


                        
                        # plot derivative
                        ax_der.semilogy(impulses_henceforth, np.abs(hpart_der), **style_hpart)
                        ax_der.semilogy(impulses_henceforth, np.abs(spectrum_der), **style_spectrum)

                        # weights
                        ax_der_weights = ax_der.twinx()
                        ax_der_weights.plot(impulses_henceforth, np.abs(weights_der), **style_weights)
                        # style
                        ax_der_weights.tick_params(
                            color=style_weights['color'],
                            labelcolor=style_weights['color']
                            )
                        


                        # cosmetics
                        ax_normal.set_title('array cavity \# {0}'.format(cavity_index))
                        
                        # yticklabels only for rightmost cavity
                        if cavity_index < N_CAVITIES-1:
                            ax_normal_weights.set_yticklabels([])
                            ax_der_weights.set_yticklabels([])
                        else:
                            ax_normal_weights.set_ylabel(r'$F_j$', color=style_weights['color'])
                            ax_der_weights.set_ylabel(r'$1 / k_0 F_j$', color=style_weights['color'])



                    legend_handles = [
                        Line2D([0], [0], **style_spectrum, label='fft'),
                        # Line2D([0], [0], **style_weights, label='weights'),
                        Line2D([0], [0], **style_hpart, label='hpart')
                        ]
                    
                    axes_der[0].legend(handles=legend_handles, loc='upper center')

                    for ax in axes_der:
                        ax.set_xlabel('$\sin \chi$')
                    
                    axes_normal[0].set_ylabel('normal')
                    axes_der[0].set_ylabel('derivative')
                    

                    plt.tight_layout()

                    fig.savefig(plottingdir + 'husimiprod_5_husimipart_weights' + img_suffix)
                    plt.close(fig)

                

                
                ####################################################################
                # complete husimi
                ####################################################################
                
                # CALCULATION
                
                if LOGGING_STEPS: print('\t\t-> calculating husimi functions')

                inout_factor = (-1)**inout_index

                husimisummand_list = []
                husimi_nearlydone_list = []
                for hpart_normal, hpart_der in zip(
                        hpart_normal_list,
                        hpart_der_list
                        ):
                    
                    # calculation
                    
                    # summands
                    husimisummand_1 = inout_factor * hpart_normal
                    husimisummand_2 = hpart_der
                    husimisummand_list.append([husimisummand_1, husimisummand_2])
                    
                    # nearlydone
                    husimi_inc_nearlydone =  husimisummand_1 + husimisummand_2
                    husimi_em_nearlydone =   husimisummand_1 - husimisummand_2
                    husimi_nearlydone_list.append([husimi_inc_nearlydone, husimi_em_nearlydone])
                    
                    # full husimis
                    husimi_inc = husimi_factor * np.abs(husimi_inc_nearlydone)**2
                    husimi_inc_list.append(husimi_inc)
                    
                    husimi_em = husimi_factor * np.abs(husimi_em_nearlydone)**2
                    husimi_em_list.append(husimi_em)
                    
                

                # PLOTTING
                if PLOTTING:
                    if LOGGING_STEPS: print('\t\t-> plotting complete husimi and constituents')
                    
                    fig, axes = plt.subplots(
                        ncols=N_CAVITIES,
                        figsize=((N_CAVITIES)*2 + 2, 2),
                        sharex='all',
                        sharey='row'
                        )
                    
                    if N_CAVITIES == 1:
                        axes_cavities = [axes]
                    else:
                        axes_cavities = axes

                    style_normal = dict(c='black', ls='solid', label=r'$h \cdot F_{0}$'.format(inout_index))
                    style_der = dict(c='black', ls='dashed', label=r"$h' \cdot i / k_0 " + r" F_{0}$".format(inout_index))
                    style_husimi_inc = dict(c='tab:red', ls='solid', label=r'$H^\mathrm{inc}' + r'_{0}$'.format(inout_index))
                    style_husimi_em = dict(c='tab:blue', ls='dashed', label=r'$H^\mathrm{em}' + r'_{0}$'.format(inout_index))



                    for ax, hpart_normal, hpart_der in zip(
                            axes_cavities,
                            hpart_normal_list,
                            hpart_der_list
                            ):


                        ax.semilogy(
                            impulses_henceforth, 
                            np.abs(hpart_normal), 
                            **style_normal
                            )
                        ax.semilogy(
                            impulses_henceforth,
                            np.abs(hpart_der),
                            **style_der
                            )
                        
                        ax.semilogy(
                            impulses_henceforth,
                            husimi_inc,
                            **style_husimi_inc
                            )
                        
                        ax.semilogy(
                            impulses_henceforth,
                            husimi_em,
                            **style_husimi_em
                            )




                    # legend
                    axes_cavities[-1].legend(
                        # loc='upper center',
                        loc='center left', 
                        bbox_to_anchor=(1, 0.5)
                        )

                    axes_cavities[0].set_ylabel(r'function value')
                    axes_cavities[0].set_title(r'single cavity')

                    for ii, ax in enumerate(axes_cavities):
                        ax.set_title(r'array cavity \# {0}'.format(ii))

                    # cosmetics
                    for ax in axes_cavities:
                        ax.set_xlabel(r'$\sin \chi$')
                        ax.set_xlim(-1,1)
                    
                    # saving
                    
                    plt.tight_layout()
                    fig.savefig(plottingdir + 'husimiprod_6_husimi_complete' + img_suffix)
                    plt.close(fig)
                

                

                ####################################################################
                # check real and complex parts of husimiparts
                ####################################################################
                if PLOTTING:
                    if LOGGING_STEPS: print('\t\t-> plotting real and imaginary components of hparts seperately')

                    fig, axes = plt.subplots(
                        nrows=3, # re, im, abs
                        ncols=N_CAVITIES,
                        figsize=(3*(N_CAVITIES),4),
                        sharex='all',
                        sharey='row'
                        )
                    
                    func_list = [np.real, np.imag, np.abs]
                    funcname_list = ['real', 'imag', 'abs']

                    if N_CAVITIES == 1:
                        axes_per_cavity = [axes]
                    else:
                        axes_per_cavity = axes.T

                    # compare

                    for cavity_index, (axes_now, summands_now, husimis_nearlydone_now) in enumerate(zip(axes_per_cavity, husimisummand_list, husimi_nearlydone_list)):
                        summand_1, summand_2 = summands_now
                        husimi_nearlydone_inc, husimi_nearlydone_em = husimis_nearlydone_now
                        
                        # plot real and imag parts linearly, the abs part logarithmically
                        for plotindex, (ax, func) in enumerate(zip(axes_now, func_list)):
                            if plotindex < 2:
                                plotfunc = ax.plot
                            else:
                                plotfunc = ax.semilogy
                            
                            # h(q,p)
                            plotfunc(
                                impulses_henceforth,
                                func(summand_1),
                                linestyle='-',
                                color='black',
                                label='part 1',
                                )
                            
                            # h'(q,p)
                            plotfunc(
                                impulses_henceforth, 
                                func(summand_2), 
                                linestyle='--',
                                color='black',
                                label='part 2'
                                )
                            
                            # added -> inc
                            plotfunc(
                                impulses_henceforth, 
                                func(husimi_nearlydone_inc), 
                                linestyle='-.',
                                label='inc (+)',
                                color='tab:red'
                                )
                            
                            # subtracted -> em
                            plotfunc(
                                impulses_henceforth, 
                                func(husimi_nearlydone_em), 
                                linestyle=':',
                                label='em (-)',
                                color='tab:blue'
                                )
                        
                        axes_now[0].set_title('cavity \#{0}'.format(cavity_index))
                    
                    
                    for ax, funcname in zip(axes_per_cavity[0], funcname_list):
                        ax.set_ylabel(funcname)
                        

                    axes_per_cavity[-1][-1].legend()
                    for axes in axes_per_cavity:
                        axes[-1].set_xlabel(r'$\sin \chi_' + inout_index_string + r'$')


                    plt.tight_layout()
                    fig.savefig(plottingdir + 'husimiprod_7_husimi_complexparts' + img_suffix)
                    plt.close(fig)

                    # return


            

            ####################################################################
            # save husimi functions
            ####################################################################
            
            if LOGGING_STEPS: print('\t-> saving husimi functions')

            # print(q_uselist)
            # print(q_uselist.shape)
            
            # define columns, columns correspond to the position in q
            # s in [0,1] -> cavity 1, s in [1,2] -> cavity 2, and so on
            q_list_complete = np.concatenate([q_uselist + ii for ii in range(N_CAVITIES)])
            # first column is list of impulses, thus p
            df_columns = ['p'] + list(q_list_complete)

            husimis_inc = np.concatenate([
                [husimi_inc_list[jj*N_CAVITIES+ii] for jj in range(n_qpoints)]
                for ii in range(N_CAVITIES)
                ])
            husimis_em = np.concatenate([
                [husimi_em_list[jj*N_CAVITIES+ii] for jj in range(n_qpoints)]
                for ii in range(N_CAVITIES)
                ])

            husimis_inc_withimpulses = np.concatenate(([impulses_henceforth], husimis_inc))
            husimis_em_withimpulses = np.concatenate(([impulses_henceforth], husimis_em))


            # inc
            df_inc = pd.DataFrame(
                data=husimis_inc_withimpulses.T,
                columns=df_columns
                )
            df_inc.to_csv(
                timestepdir_now + 'husimi_{0}_inc.csv'.format(inout_index),
                index=False
                )
            
            # em
            df_em = pd.DataFrame(
                data=husimis_em_withimpulses.T,
                columns=df_columns
                )
            df_em.to_csv(
                timestepdir_now + 'husimi_{0}_em.csv'.format(inout_index),
                index=False
                )
            


            ####################################################################
            # quick plot of the husimi functions
            ####################################################################
            
            if LOGGING_STEPS: print('\t\t-> plotting husimi functions')


            for husimi_use, wavedirection in zip(
                    [husimis_inc, husimis_em],          # data to use
                    ['inc', 'em'],                      # label plot if incident or emerging waves
                    
                    ):
                
                plot_husimi(
                    q_list_complete, impulses_henceforth, husimi_use.T,
                    inout_index=inout_index, 
                    wave_direction=wavedirection, 
                    savedir=timestepdir_now,
                    n_cavities=N_CAVITIES,
                    index_inner=INDEX_INNER,
                    )
        

        # if end after 1 timestep
        # return

    # end
    return




####################################################################
# useful functions
####################################################################


def complete_fft(x:np.ndarray, y:np.ndarray) -> np.ndarray:
    '''
    calculate fft of data and returns spectrum & frequencies
    '''
    x_delta = np.mean(x[1:] - x[:-1])
    freqs = np.fft.fftshift(np.fft.fftfreq(x.shape[0], d=x_delta))
    
    # determine spectra
    fourier_spectrum = np.fft.fft(y)
    fourier_spectrum = np.fft.fftshift(fourier_spectrum)
                
    return [freqs, fourier_spectrum]



# coherent_state_sharpness = 30   # sharpness of peak -> maybe chack influence of this parameter
q_delta = 0.5                   # determines shift of curve, so data is always
                                # centered around q
coherent_state_sigma = np.sqrt(.05) / 20.                    # standard deviation
coherent_state_potfac = 1. / (2.*coherent_state_sigma)              # factor for (s-q)**2
coherent_state_allfac = np.power(coherent_state_sigma*np.pi, -0.25) # factor for the whole thing
# print('coherent state : {0:.6f}, {1:.6f}, {2:.6f}'.format(coherent_state_sigma, coherent_state_potfac, coherent_state_allfac))
def coherent_state(
    s: np.ndarray, 
    q: np.ndarray
    ) -> np.ndarray:
    '''
    coherent state with periodic boundary conditions, concentrated around q
    '''
    
    # old
    # return np.exp( - np.square(s-q) * coherent_state_sharpness )
    
    # new
    return np.exp( - np.square(s-q) * coherent_state_potfac) * coherent_state_allfac



plotted_coherence = False
def apply_coherent_state(
    q: float, 
    s: np.ndarray, 
    y: np.ndarray, 
    repeat:int=0,     # repeates the signal, practically useless
    broaden:int=0     # adds s points where field is virtually 0, making the trafo smoother
    ) -> np.ndarray:
    '''
    applies coherent state at q to the function y at coordinates s
    '''
    
    # only one option can be active at a time
    if repeat > 0 and broaden > 0:
        print('ERROR: coherent state only can use one option at a time')
        raise ValueError
    
    
    # shift of s coordinates
    if q < 0.5: # shift to the left
        s_max = q + q_delta
        s_bool = s > s_max
        
        s_left = s[np.logical_not(s_bool)]
        s_right = s[s_bool]
        s_new = np.concatenate(( s_right - 1., s_left))
        
        # shift of y coordinates
        y_right = y[s_bool]
        y_left = y[np.logical_not(s_bool)]
    else:       # shift to the right
        s_min = q - q_delta
        s_bool = s < s_min
        
        s_left = s[s_bool]
        s_right = s[np.logical_not(s_bool)]
        s_new = np.concatenate((s_right, s_left + 1.))
    
        # shift of y coordinates
        y_left = y[s_bool]
        y_right = y[np.logical_not(s_bool)]
        
    y_new = np.concatenate((y_right, y_left))
    
    # adds new datapoints, where field is virtually zero due to the
    # shape of the gaussian of the coherent state
    if broaden > 0:
        # the +1 is needed so for broaden=1 -> signal gets repeated 3 times
        no_of_signals = broaden*2+1 # broaden number of lines get added to each side
        s_new = np.concatenate([s_new + ii for ii in np.arange(no_of_signals)]) - broaden
        y_new = np.concatenate([y_new for ii in np.arange(no_of_signals)])
    

    # multiplication with coherent state
    # neighboring coherent states are seen, identical to repeating signal
    # coherent_states = [coherent_state(s_new, q+ii) for ii in np.arange(-2,3)]
    # out = y_new * np.sum(coherent_states, axis=0)

    # apply coherent state to new data found by broaden
    out = coherent_state(s_new, q) * y_new


    
    # repeating the signal so plots look nicer
    # but it is practically useless
    if repeat > 0:
        s_listlist = []
        out_listlist = []
        
        for i in range(repeat):
            s_listlist.append(s_new + i)
            out_listlist.append(out)
        
        s_new = np.concatenate(s_listlist)
        out = np.concatenate(out_listlist)
            
    
    # output
    return np.array([s_new, out])
    


def calculate_hpart(q: np.ndarray, x: np.ndarray, y: np.ndarray, broaden=None) -> np.ndarray:
    '''
    Calculates part of husimi function, which are then to be combined
    to form the hinal husimi functions.
    
    dimensions : (q, p)
    '''
    
    hpart = []
    
    for q_now in q:
        _, y_new = apply_coherent_state(q_now, x, y, broaden=broaden)
        spectrum = np.fft.fftshift(np.fft.fft(y_new))
        hpart.append(spectrum)
    
    return np.array(hpart)



def cut_data(
    s_list:np.ndarray,
    y_list:np.ndarray, 
    ncavities:int=1
    ) -> np.ndarray:
    '''
    cuts data in for each seperate microcavity so coherent states can be applied individually
    '''
    
    # for 1 cavity do nothing
    if ncavities == 1:
        return np.array([[s_list], [y_list]])


    # else check s indices
    s_listlist = []
    y_listlist = []
    for i in range(ncavities):
        s_abs_diflist = s_list - i
        s_bool_list = (0. <= s_abs_diflist) & (s_abs_diflist < 1.)

        s_list_now = s_list[s_bool_list]
        y_list_now = y_list[s_bool_list]

        s_listlist.append(s_list_now)
        y_listlist.append(y_list_now)
    
    return np.array([s_listlist, y_listlist])


####################################################################
# husimi plotting function
####################################################################

def plot_husimi(
        q, p, husimi_data, 
        inout_index=0, 
        wave_direction='inc', 
        savedir='.', 
        n_cavities=1, 
        index_inner=None, 
        extrasuffix='',
        vmin=0.,
        vmax=None,
        nlevels=10
        ):
    
    # initialize
    fig, ax = plt.subplots(
        figsize=(3*n_cavities+.5, 3)
        )


    # plotting

    # level definition, so levels can be controlled better
    # e.g. when multiple plots are to have the same levels
    if vmax is None:
        vmax = np.max(husimi_data)
    levels = np.linspace(vmin, vmax, nlevels+1)

    im = ax.contourf(
        q,
        p,
        husimi_data,
        origin='lower',
        cmap='Reds',
        vmin=vmin,
        vmax=vmax,
        levels=levels
        )

    colorbar_label = r'$H^\mathrm{' + r'{0}'.format(wave_direction) + r'}' + r'_{0}$'.format(inout_index)
    fig.colorbar(im, label=colorbar_label)


    # markings

    for x in np.arange(1, n_cavities):
        ax.axvline(x, color='black')

    if index_inner and inout_index == 1:
        angle_crit = 1. / index_inner
        for y in [-angle_crit, angle_crit]:
            ax.axhline(y, color='black', linestyle='--')

    # cosmetics

    ax.set_yticks([-1.,-.5, .0, .5, 1.])

    ax.set_xlim(0, n_cavities)
    ax.set_ylim(-1,1)

    ax.set_xlabel(r'$s$')
    ax.set_ylabel(r'$\sin \chi_{0}$'.format(inout_index))
    
    # saving

    figtitle = '/husimi_{0}_{1}'.format(inout_index, wave_direction) + extrasuffix + '.png'

    fig.savefig(savedir + figtitle)
    plt.close(fig)


if __name__ == '__main__':
    main()