import numpy as np
import os
from scipy.signal import find_peaks

import params as pr

import plotparams
import matplotlib.pyplot as plt


def main():
    print('calculating radial quantum number')
    
    print('\t- loading params')
    params = pr.load_useparams(fname='output/params_used.json')
    
    OUTPUTDIR = params['outputfolder']
    N_CAVITIES = params['n_cavities']
    cmap = plotparams.cmap
    
    # load data
    
    outputdir_period = 'borderfunctions_period/'
    fnames = sorted(
        [
            f for f in os.listdir(OUTPUTDIR + outputdir_period)
            if f[-4:] == '.dat' and f.split('_')[0] == 'boundaryfunc'
        ]
        )
    n_timesteps = len(fnames)
    
    
    # initialize
    abs_spectrum_all = None
    
    for timeindex in range(n_timesteps):
        s_list, boundaryfunc, _, _ = np.loadtxt(OUTPUTDIR + outputdir_period + 'boundaryfunc_{0:03d}.dat'.format(timeindex))
        
        
        spectrum = np.fft.rfft(boundaryfunc)
        abs_spectrum = np.abs(spectrum)
        
        if abs_spectrum_all is None:
            abs_spectrum_all = abs_spectrum
        else:
            abs_spectrum_all += abs_spectrum
    
    
    
    # finding peaks
    
    # meaning spectrum
    abs_spectrum_all = abs_spectrum_all / n_timesteps
    # finding locations of peaks
    peak_locs, peak_dicts = find_peaks(abs_spectrum_all)
    # reading heights of peaks
    peak_heights = abs_spectrum_all[peak_locs]
    
    # obtain highest peak
    maxpeak_index = np.argmax(peak_heights)
    maxpeak_loc = peak_locs[maxpeak_index]
    maxpeak_value = peak_heights[maxpeak_index]
    
    quantum_number = int(maxpeak_loc / N_CAVITIES)
    
    np.savetxt(
        OUTPUTDIR + 'radial_quantumnumber.dat', 
        [quantum_number],
        fmt='%i'
        )
    
    if pr.PLOTTING:
        fig, axes = plt.subplots(nrows=2)

        # data ax

        ax_data = axes[0]
        
        ax_data.plot( # data of last timestep
            s_list, boundaryfunc
            )
        
        
        # trafo ax
        
        ax_trafo = axes[1]
        
        ax_trafo.plot(peak_locs / N_CAVITIES, peak_heights, color='tab:blue', marker='o', linestyle=' ', label='local maximum', zorder=2)
        
        ax_trafo.axvline(maxpeak_loc / N_CAVITIES, color='red', label='global maximum', zorder=4, linestyle='dashed')
        
        ax_trafo.plot(
            np.arange(len(abs_spectrum_all)) / N_CAVITIES,
            abs_spectrum_all, 
            color='black', 
            label='data', 
            zorder=3
            )
        
        ax_trafo.text(
            quantum_number, 1, '$m={0}$'.format(quantum_number),
            ha='center', va='bottom',
            rotation=90,
            zorder=10,
            bbox=dict(
                edgecolor='black',
                boxstyle='round',
                facecolor='white'
                )
            
            )
        
        ax_trafo.set_xlim(quantum_number-10,quantum_number+10)
        
        ax_trafo.set_xlabel('\# of oscillations')
        ax_trafo.set_ylabel('mean value of fourier coeff [a.u.]')
        
        ax_trafo.legend()
        
        fig.savefig(OUTPUTDIR + 'radial_quantum_number.png')
        plt.close(fig)
        




if __name__ == '__main__':
    main()